

# HomeToDo 🏠📅

HomeToDo allows the users to create and join a virtual home shared with other people.
The web-app will provide a shared calendar for the group of people in a home, where each person can either add duties or a payment that needs to be shared with other home-member.




## Authors ✒️

* **Alessandra Rota** - 829775
* **Andrea Mariotti** - 829534
* **Beatrice Stropeni** - 830017
* **Davide Piovani** - 830113
* **Sofia Zonari** - 829741



## Documentation

Documentation for the project can be found at: https://arota54.gitlab.io/hometodo/



## High Level Architecture

![High Level Architecture](images/LabProgettazione - High Level Architecture.png)

Our project is based on the Spring Boot framework.
The model package, containing our Entities, interacts with a H2 database using the Hibernate ORM Library. The Repository instances an EntityManager to execute the queries on the database; there's a Repository for each functional component.
The Controller package consists of two subset of classes for each functional component:

- the REST controllers, whose duty is to retrieve information from the REST API and deliver this information to the Repository
- the standard controllers, whose duty is to redirect the user to frontend pages.

The View contains the templates for rendering the frontend pages (HTML + CSS  + JS + Bootstrap).
The Config package has been created to handle the security components. 



## DB Model

![DB Model](images/image-20220516110851277.png)



## Mockup (1st Sprint version)

![Mockup-1](images/Mockup-1.jpg)


## Mockup (2st Sprint version)

![Mockup-2](images/Mockup-2.jpg)


## APIs 

### GET requests

* **/join-via-link/{join_id}**: shows the first login page and lets the user choose between loggin in right away or sign up

* **/join-via-link/{join_id}/register**: returns the view containing the register form, allows to keep the join id as a parameter

* **/add/user-in-a-home**: 
    * returns the view showing the home information if the user it is not an administrator
    * returns the view of the login if the user is not logged
    * return the view to add a user in a home otherwise

* **/home**: returns the view showing the information of the logged user's home

* **/home/add**: returns the view containing the form to add a new home

* **/home/{id}/taskcount**: gets duty counts split by HomeTask for all users in a given Home

* **/home/hometasks**: gets all the HomeTasks in a Home

* **/home/users**: gets all the Users in a Home

* **/schedule**: returns the view showing the home schedule if the user belongs to a home

* **/schedule/month/{date}**: gets all the duties of a Home in a month

* **/schedule/week/{date}**: gets all the duties of a Home in a week

* **/schedule/day/{date}**: gets all the duties of a Home in a day

* **/schedule/{id}**: gets the detail of a given duty

* **/login**: returns the view containing the form to login

* **/resetPassword**: returns the view which asks for the email of the user of which the password needs to be reset

* **/register**: returns the view containing the register form

### POST requests

* **/join-via-link/{join_id}**: maps the login functionality and redirects to the correct page, either error or the home page of the home who the user has just joined

* **/join-via-link/{join_id}/register**: returns the view containing the result of the registration and signs up the user

* **/add/user-to-home**: adds the user to a home, if it is possibile

* **/add/user-in-a-home/get-all-users-with-no-home**: returns all the users without a home

* **/duty/{id}/edit**

* **/duty/add**

* **/home/add**

* **/login**

* **/navbar**: returs if the User is a User in a home or not to change the navbar component

* **/resetPassword**: collects all data to be passed to /securityQuestion

* **/resetPassword**: returns a view were is shown the result of the change of password

* **/register**



### DELETE requests

* **/home/users/{id}/remove**: removes a user with the specified id from its Home. Request must be started from the Home admin
