package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test class for DutyRepositoryImpl
 */
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class DutyRepositoryImplTest {

    /**
     * Checks if a Duty can be found by id
     */
    @Test
    void findById() {
        HomeTask ht=createDB();

        Optional<Duty> duty = DutyRepositoryImpl.getInstance().findById(1);
        assertTrue(duty.isEmpty() == true);
    }

    /**
     * Checks if a Duty can be found by a date range
     */
    @Test
    void findByDateRange() {
        HomeTask ht=createDB();
        List<HomeTask> hts = new ArrayList<>();
        hts.add(ht);

        List<Duty> duties = (List<Duty>) DutyRepositoryImpl.getInstance().findByDateRange("2022-04-03", "2022-04-04",hts);
        assertTrue(duties.size() == 2);
    }

    /**
     * Checks if a Duty can be found by a specific date
     */
    @Test
    void findByDate() {
        HomeTask ht=createDB();
        List<HomeTask> hts = new ArrayList<>();
        hts.add(ht);

        List<Duty> duties = (List<Duty>) DutyRepositoryImpl.getInstance().findByDate("2022-04-03", hts);
        assertTrue(duties.size() == 1);
    }

    /**
     * Populates the db with test objects
     */
    private HomeTask createUserDB(){
        //User u= new User("1","a","a","a","a",null);
        User u = new User("1","a","a","a","a");
        ArrayList<User> userList = new ArrayList<>();
        userList.add(u);
        UserRepositoryImpl.getInstance().insert(u);
        Home h= new Home("MockHomeName", u,null);
        HomeRepositoryImpl.getInstance().insert(h);
        HomeTask ht= new HomeTask("ht",h);
        HomeTaskRepositoryImpl.getInstance().insert(ht);
        insertDuty(new Date(122,3,2),ht, userList);
        insertDuty(new Date(122,3,3),ht, userList);
        insertDuty(new Date(122,3,4),ht, userList);
        return ht;
    }

    private HomeTask createDB(){
        //User u= new User("1","a","a","a","a",null);
        User u = new User("1","a","a","a","a");
        UserRepositoryImpl.getInstance().insert(u);
        Home h= new Home("MockHomeName", u,null);
        HomeRepositoryImpl.getInstance().insert(h);
        HomeTask ht= new HomeTask("ht",h);
        HomeTaskRepositoryImpl.getInstance().insert(ht);
        insertDuty(new Date(122,3,2),ht, null);
        insertDuty(new Date(122,3,3),ht, null);
        insertDuty(new Date(122,3,4),ht, null);
        return ht;
    }

    /**
     * Inserts a Duty in the db with the specified parameters
     * @param date
     * @param ht
     */
    private void insertDuty(Date date, HomeTask ht, ArrayList<User> users){
        Duty d= new Duty(date, false, "n", ht, users);
        DutyRepositoryImpl.getInstance().insert(d);
    }

    /**
     * Checks if the number of tasks is correctly displayed
     */
    @Test
    public void checkIfCountsDutiesByHomeTask(){
        HomeTask ht = createUserDB();

        List<Object[]> counts = DutyRepositoryImpl.getInstance().countDutiesByHomeTask("1");
        assertTrue(counts.size() == 1, "There shouldn't be more than one item in task counts");
        HomeTask count_ht = (HomeTask) counts.get(0)[0];
        Long count_num = (Long) counts.get(0)[2];

        assertEquals(ht.getId(), count_ht.getId(), "Home task is not in counts");
        assertEquals(3, count_num.intValue(), "There should be 3 duties associated to a home task");
    }

    /**
     * Checks if no count is retrieved if there's no task.
     */
    @Test
    public void checkIfNoCountsDutiesByHomeTaskIfNoDuties(){
        HomeTask ht = createDB();

        List<Object[]> counts = DutyRepositoryImpl.getInstance().countDutiesByHomeTask("1");
        assertTrue(counts.size() == 0, "There shouldn't be items in task counts");
    }

    /**
     * Checks if home useres are corrctly updated
     */
    @Test
    public void updateDutyUsers() {
        HomeTask ht = createUserDB();
        List<HomeTask> hts = new ArrayList<>();
        hts.add(ht);

        Optional<User> optUser = UserRepositoryImpl.getInstance().findById("1");
        List<Duty> duties = (List<Duty>) DutyRepositoryImpl.getInstance().findByDate("2022-04-03", hts);
        Duty duty = duties.get(0);

        duty.deleteUser(optUser.get());
        DutyRepositoryImpl.getInstance().update(duty);

        List<Duty> upDuties = (List<Duty>) DutyRepositoryImpl.getInstance().findByDate("2022-04-03", hts);
        Duty upDuty = upDuties.get(0);

        assertNotEquals(upDuty, duty, "Duties should be different!");
        assertEquals(upDuty.getUsers().size(), 0, "Duty should have no user!");
    }

}
