package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Duty;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.HomeTask;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test class for HomeRepositoryImpl
 */
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class HomeRepositoryImplTest {

    /**
     * Checks if user is correctly associated to a home
     */

    @Test
    void IfSetHomeUserInHome() {
        Home home = new Home();
        User user = new User();

        user.setName("sofia");
        user.setSurname("zonari");
        user.setEmail("s@sofia.com");

        home.setName("casa");
        home.setAdministrator(user);
        home.setUsers(new ArrayList());

        UserRepositoryImpl.getInstance().insert(user);
        home.addUser(user);

        HomeRepositoryImpl.getInstance().insert(home);
        List<Home> homes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("s@sofia.com");
        //user.setHome(homes.get(0));
        UserRepositoryImpl.getInstance().update(user);

        Optional<User> updated_user = UserRepositoryImpl.getInstance().findById(user.getEmail());
        assertEquals(true, updated_user.isPresent(), "User should be in the database");

        // user's home
        List<Home> updatedUserHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.getEmail());
        Home updatedUserHome = updatedUserHouses.get(0);

        assertEquals(homes.get(0).getId(), updatedUserHome.getId(), "Home id should be the same");
    }

    /**
     * Checks if a home can be found by its admin
     */
    @Test
    void IfUserIsAdminFindHome() {

        createDB();

        List<Home> homes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin("s@sofia.com");
        assertEquals(1, homes.size(), "Only one home should be returned");
        assertEquals(homes.get(0).getAdministrator().getEmail(), "s@sofia.com", "The home should be the one assigned to admin");
    }

    /**
     * Checks if a home can be found by its user
     */
    @Test
    void IfUserInHomeFindHomeByUser() {
        createDB();

        List<Home> homes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("s@sofia.com");
        assertEquals(1, homes.size(), "Only one home should be returned");
        assertEquals(homes.get(0).getUsers().get(0).getEmail(), "s@sofia.com", "The home should be the one assigned to user");
    }


    /**
     * Checks if no home can be found if the user is not in a home
     */
    @Test
    void IfUserNotInHomeFindNoHomes() {
        createDB();

        List<Home> homes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("e@elena.com");
        assertEquals(0, homes.size(), "No homes should be retrieved");
    }


    /**
     * Check if the user is correctly removed from home
     */
    @Test
    public void updateHomeUsers() {
        createDB();
        Home home = ((List<Home>) HomeRepositoryImpl.getInstance().findByUser("s@sofia.com")).get(0);
        User user = UserRepositoryImpl.getInstance().findById("s@sofia.com").get();
        home.deleteUser(user);
        HomeRepositoryImpl.getInstance().update(home);


        Optional<Home> upHome = HomeRepositoryImpl.getInstance().findById(home.getId());

        assertNotEquals(home, upHome, "Homes should be different!");
        assertEquals(upHome.get().getUsers().size(), 0, "Home should have no user!");
    }

    /**
     * Populates the DB with a Home and a User for tests
     */
    private void createDB() {
        Home home = new Home();
        User user = new User();

        user.setName("sofia");
        user.setSurname("zonari");
        user.setEmail("s@sofia.com");

        home.setName("casa");
        home.setAdministrator(user);
        home.setUsers(new ArrayList());

        UserRepositoryImpl.getInstance().insert(user);
        home.addUser(user);
        HomeRepositoryImpl.getInstance().insert(home);
        return;
    }
}