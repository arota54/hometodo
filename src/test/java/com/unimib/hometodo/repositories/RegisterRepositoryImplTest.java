package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Unit test class for RegisterRepositoryImpl
 */
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class RegisterRepositoryImplTest {

    User testUser;


    public RegisterRepositoryImplTest() {
        testUser = new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");

    }

    /**
     * Checks for a control on an already existent email
     */
    @Test
    void isEmailAlreadyInUse() {
        Assertions.assertFalse(RegisterRepositoryImpl.getInstance().isEmailAlreadyInUse(testUser));
        RegisterRepositoryImpl.getInstance().register(testUser);
        Assertions.assertTrue(RegisterRepositoryImpl.getInstance().isEmailAlreadyInUse(testUser));

    }

    /**
     * Checks if a user can be registered and if it cannot be registered twice
     */
    @Test
    void register() {
        Assertions.assertTrue(RegisterRepositoryImpl.getInstance().register(testUser));
        Assertions.assertFalse(RegisterRepositoryImpl.getInstance().register(testUser));

    }

    /**
     * Checks if constraints on password are fully verified
     */
    @Test
    void respectsPasswordConstraint() {
        Assertions.assertFalse(RegisterRepositoryImpl.getInstance().respectsPasswordConstraint("ciao"));
        Assertions.assertFalse(RegisterRepositoryImpl.getInstance().respectsPasswordConstraint("Ciao"));
        Assertions.assertFalse(RegisterRepositoryImpl.getInstance().respectsPasswordConstraint("Ci23ao"));
        Assertions.assertTrue(RegisterRepositoryImpl.getInstance().respectsPasswordConstraint("Ci23a!o"));
    }

    /**
     * Checks if the password is changed
     */
    @Test
    void changePassword() {
        RegisterRepositoryImpl.getInstance().register(testUser);
        User tempUser = new User();
        tempUser.setName("davide");
        tempUser.setSurname("piovani");
        tempUser.setPassword("newPassword!1");
        tempUser.setSecurity_question("1");
        tempUser.setSecurity_question_answer("pollo");
        tempUser.setEmail("piovanid@gmail.com");
        tempUser.setHash("testHash");
        Assertions.assertTrue(RegisterRepositoryImpl.getInstance().changePassword(testUser));

    }
}


