package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Unit test class for LoginRepositoryImpl
 */
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class LoginRepositoryImplTest {

    User testUser;

    public LoginRepositoryImplTest() {
        testUser = new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");
    }

    /**
     * Checks for setting an hash to a user
     */
    @Test
    void setHash() {
        User tempUser =null;
        String testHash="testHash";
        RegisterRepositoryImpl.getInstance().register(testUser);
        LoginRepositoryImpl.getInstance().setHash(testUser, testHash);
        Optional<User> optionalUser =UserRepositoryImpl.getInstance().findById(testUser.getEmail());
        if (optionalUser.isPresent()){
            tempUser =optionalUser.get();
        }else
            return;
        Assertions.assertEquals(testHash, tempUser.getHash());
        //todo remove users

    }

    /**
     * Checks if credentials are correctly handled
     */
    @Test
    void checkCredentials() {

        //test a matching user
        Assertions.assertTrue(RegisterRepositoryImpl.getInstance().register(testUser));
        User debugUser=new User();
        debugUser.setName("davide");
        debugUser.setSurname("piovani");
        debugUser.setPassword("Password!1");
        debugUser.setSecurity_question("1");
        debugUser.setSecurity_question_answer("pollo");
        debugUser.setEmail("piovanid@gmail.com");
        debugUser.setHash("testHash");

        Assertions.assertTrue(LoginRepositoryImpl.getInstance().checkCredentials(debugUser));

        //test wrong user
        User fakeUser = new User();
        fakeUser.setName("davide");
        fakeUser.setSurname("piovani");
        fakeUser.setPassword("FakePassword!1");
        fakeUser.setSecurity_question("1");
        fakeUser.setSecurity_question_answer("pollo");
        fakeUser.setEmail("piovanid@gmail.com");
        fakeUser.setHash("testHash");
        Assertions.assertFalse(LoginRepositoryImpl.getInstance().checkCredentials(fakeUser));

    }


}