package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Duty;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.HomeTask;
import com.unimib.hometodo.models.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Unit test class for HomeTaskRepositoryImpl
 */
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class HomeTaskRepositoryImplTest {

    /**
     * Checks if a HomeTask can be found by id
     */
    @Test
    void findById() {
        HomeTask ht = createDB();

        Optional<HomeTask> homeTask = HomeTaskRepositoryImpl.getInstance().findById(ht.getId());
        assertTrue(homeTask.isPresent());
    }

    /**
     * Checks if it return all the HomeTask in a Home
     */
    @Test
    void findAllHomeTaskInAHome() {
        HomeTask ht = createDB();

        List<HomeTask> homeTasks = (List<HomeTask>) HomeTaskRepositoryImpl.getInstance().findByHome(ht.getHome());
        assertEquals(1, homeTasks.size());
    }

    /**
     * Populates the db with test objects
     */
    private HomeTask createDB() {
        User u = new User("1","a","a","a","a");
        UserRepositoryImpl.getInstance().insert(u);
        Home h = new Home("MockHomeName", u,null);
        HomeRepositoryImpl.getInstance().insert(h);

        h.addUser(u);
        HomeRepositoryImpl.getInstance().update(h);

        HomeTask ht= new HomeTask("ht", h);
        HomeTaskRepositoryImpl.getInstance().insert(ht);
        return ht;
    }
}
