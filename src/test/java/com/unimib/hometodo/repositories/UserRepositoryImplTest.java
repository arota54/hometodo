package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test class for HomeRepositoryImpl
 */
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class UserRepositoryImplTest {

    User testUser;


    public UserRepositoryImplTest() {
        testUser = new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");

    }

    /**
     * Checks if the user retrieved form the cookie corresponds
     */
    @Test
    void getUserByCookie() {
        RegisterRepositoryImpl.getInstance().register(testUser);
        User resultUser = UserRepositoryImpl.getInstance().GetUserByCookie("piovanid@gmail.com-testHash");
        Assertions.assertEquals(resultUser.getEmail(), testUser.getEmail());
        resultUser = UserRepositoryImpl.getInstance().GetUserByCookie("notpiovanid@gmail.com-testHash");
        Assertions.assertEquals(resultUser, null);
    }

    /**
     * Test of the function getUserInAHomeByEmail of the UserRepositoryImpl class
     */
    @Test
    public void getUserInAHomeByEmailAndHashTest() {
        // The user has no home
        getUserInAHomeByEmailAndHasInsertedWithNoHome();

        // The user has a home
        getUserInAHomeByEmailAndHashInsertedWithAHome();

        // Search the user with a home and with the wrong hash
        getUserInAHomeByEmailAndHashWrongHash1();

        // Search the user with a home and with the wrong hash
        getUserInAHomeByEmailAndHashWrongHash2();

        // Search the user with no home and with the wrong hash
        getUserInAHomeByEmailAndHashNoHomeWrongHash();
    }

    /**
     * Checks if user is correctly retrieved, given its email and its home
     */
    @Test
    public void getUserIfInAHome() {
        Home home = new Home();
        home.setName("aaa");
        User userWithHome = insertUser("pippo@gmail.com",
                "Luca",
                "Colombo",
                "psw",
                null,
                home);
        home.setAdministrator(userWithHome);
        home.addUser(userWithHome);

        Home found = ((List<Home>) HomeRepositoryImpl.getInstance().findByUser(userWithHome.getEmail())).get(0);
        Optional<User> optUser = UserRepositoryImpl.getInstance().getUserInAHome(userWithHome.getEmail(), found.getId());
        assertEquals(optUser.get(), userWithHome, "User should be in the home!" );
    }

    /**
     * Checks if user is not retrieved, given its email, if it doesn't belong to a home
     */
    @Test
    public void getNoUserIfNotAHome() {
        Home home = new Home();
        home.setName("aaa");

        User userWithHome = insertUser("pippo@gmail.com",
                "Luca",
                "Colombo",
                "psw",
                null,
                home);
        home.setAdministrator(userWithHome);
        home.addUser(userWithHome);

        Optional<User> optUser = UserRepositoryImpl.getInstance().getUserInAHome("pluto@gmail.com", 1);
        assertFalse(optUser.isPresent(),"User shouldn't be in the home!" );
    }


    /**
     * Create User with a Home which has a particular home name
     * @param email
     * @param password
     * @param hash
     * @param name
     * @param surname
     * @param homeName
     * @return
     */
    private User createUserInAHome(String email, String password, String hash, String name, String surname, String homeName){
        Home home =  new Home(homeName);

        User user = new User(email, password, hash, name, surname, null, null);

        home.setAdministrator(user);
        home.addUser(user);

        return user;

        //return new User(email, password, hash, name, surname, null, null, home);
    }


    /**
     * Create a new User object set with input parameters and insert the user into DB.
     * @param email
     * @param password
     * @param hash alphanumeric string
     * @param name
     * @param surname
     * @param home
     * @return the User insert into DB
     */
    private User insertUser(String email, String password, String hash, String name, String surname, Home home){
        //User user = new User(email, password, hash, name, surname, null, null, home);

        //User user = new User(email, password, hash, name, surname, null, null);

        User user = new User(email, name, surname, password, null, null, hash);

        UserRepositoryImpl impl = UserRepositoryImpl.getInstance();
        impl.insert(user);

        if(home != null) {
            home.addUser(user);
            HomeRepositoryImpl.getInstance().insert(home);
        }

        return user;
    }


    /**
     * Test user with no home
     */
    private void getUserInAHomeByEmailAndHasInsertedWithNoHome() {
        // Insert a user with no home and then try to get it by email and hash
        User userInserted = insertUser("email@gmail.com",
                "psw12",
                "hash123",
                "aaa",
                "ccc",
                null);

        // Search a user with a home, but the user inserted in the db doesn't have one
        User userGot = UserRepositoryImpl.getInstance().getUserInAHomeByEmailAndHash(userInserted.getEmail(),
                userInserted.getHash());

        // The user has not home so userGot has to be null
        Assertions.assertEquals(userGot, null);
    }


    /**
     * Test user with home
     */
    private void getUserInAHomeByEmailAndHashInsertedWithAHome() {
        // Insert a user with a home and then try to get it by email and hash
        /*User userWithHome = createUserInAHome("pippo@gmail.com",
                "psw",
                "h12345",
                "Luca",
                "Colombo",
                "NomeCasa1");*/

        // Insert a user with a home and then try to get it by email and hash
        User userWithHome = new User("pippo@gmail.com",
                "Luca",
                "Colombo",
                "psw",
                null,
                null,
                "h12345");
        Home home = new Home("NomeCasa1");
        home.setAdministrator(userWithHome);
        home.addUser(userWithHome);

        // Insert the user who has already set a home
        UserRepositoryImpl.getInstance().insert(userWithHome);

        // Insert house
        //HomeRepositoryImpl.getInstance().insert(userWithHome.getHome());
        HomeRepositoryImpl.getInstance().insert(home);



        // Search a user with a home and the user inserted in the db has one
        User userGot = UserRepositoryImpl.getInstance().getUserInAHomeByEmailAndHash(userWithHome.getEmail(),
                userWithHome.getHash());

        // The user has a home so userGot has to be not null
        Assertions.assertNotEquals(userGot, null);

        // The user has to be the same we have inserted
        Assertions.assertEquals(userGot, userWithHome);
    }


    /**
     * Test user with wrong hash test 1
     */
    private void getUserInAHomeByEmailAndHashWrongHash1() {
        // Search a user that is in the db and has a home, but with the wrong hash
        /*User userWithHome = createUserInAHome("baudo@gmail.com",
                "psw123",
                "h12",
                "Andrea",
                "Colomboa",
                "NomeCasa2");*/

        // Search a user that is in the db and has a home, but with the wrong hash
        User userWithHome = new User("baudo@gmail.com",
                "Andrea",
                "Colomboa",
                "psw123",
                null,
                null,
                "h12");
        Home home = new Home("NomeCasa2");
        home.setAdministrator(userWithHome);
        home.addUser(userWithHome);

        // Insert the user who has already set a home
        UserRepositoryImpl.getInstance().insert(userWithHome);

        // Insert house
        //HomeRepositoryImpl.getInstance().insert(userWithHome.getHome());
        HomeRepositoryImpl.getInstance().insert(home);

        // Search a user with a home and the user inserted in the db has one, but with the wrong hash
        User userGot = UserRepositoryImpl.getInstance().getUserInAHomeByEmailAndHash(userWithHome.getEmail(),
                "hashsbagliato");

        // The user has a home so userGot has to be not null
        Assertions.assertEquals(userGot, null);
    }


    /**
     * Test user with wrong hash test 2
     */
    private void getUserInAHomeByEmailAndHashWrongHash2() {
        // Search a user that is in the db and has a home, but with the wrong hash
        /*User userWithHome = createUserInAHome("davide@gmail.com",
                "psw34iywo8",
                "h12uyfuih",
                "Davude",
                "Colomb",
                "NomeCasa3");*/

        // Insert a user with a home and then try to get it by email and hash
        User userWithHome = new User("davide@gmail.com",
                "Davude",
                "Colomb",
                "psw34iywo8",
                "Colomb",
                null,
                "h12uyfuih");
        Home home = new Home("NomeCasa3");
        home.setAdministrator(userWithHome);
        home.addUser(userWithHome);

        // Insert the user who has already set a home
        UserRepositoryImpl.getInstance().insert(userWithHome);

        // Insert house
        //HomeRepositoryImpl.getInstance().insert(userWithHome.getHome());
        HomeRepositoryImpl.getInstance().insert(home);

        // Search a user with a home and the user inserted in the db has one but with the wrong hash
        User userGot = UserRepositoryImpl.getInstance().getUserInAHomeByEmailAndHash(userWithHome.getEmail(), "");

        // The user has a home so userGot has to be not null
        Assertions.assertEquals(userGot, null);
    }


    /**
     * Test user with no home with the wrong hash
     */
    private void getUserInAHomeByEmailAndHashNoHomeWrongHash() {
        // Search a user that is in the db and hasn't a home, but with the wrong hash
        // Insert house with administrator
        Home home = new Home("NomeCasa2");
        HomeRepositoryImpl.getInstance().insert(home);

        /*User userWithHome = new User("email33@gmail.com",
                "psw123",
                "aaa12",
                "Paige",
                "Smith",
                null, null,
                home);*/

        User userWithHome = new User("email33@gmail.com",
                "Paige",
                "Smith",
                "psw123",
                null,
                null,
                "aaa12");
        home.setAdministrator(userWithHome);
        home.addUser(userWithHome);

        //Update the user with the home
        //UserRepositoryImpl.getInstance().update(userWithHome);
        UserRepositoryImpl.getInstance().update(userWithHome);
        HomeRepositoryImpl.getInstance().update(home);

        // Insert of a UH who is in the house
        User userInserted = insertUser("email5@gmail.com",
                "psw123",
                "aaa12",
                "Paige",
                "Smith",
                home);

        // Search a user with a home and the user inserted in the db has one but with the wrong hash
        User userGot = UserRepositoryImpl.getInstance().getUserInAHomeByEmailAndHash(userInserted.getEmail(),
                "");

        // The user has a home so userGot has to be not null
        Assertions.assertEquals(userGot, null);
    }
}