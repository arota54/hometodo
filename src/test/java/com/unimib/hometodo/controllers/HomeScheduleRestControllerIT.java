package com.unimib.hometodo.controllers;

import com.unimib.hometodo.config.WithMockCustomUser;
import com.unimib.hometodo.models.*;
import com.unimib.hometodo.repositories.*;
import org.json.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

/**
 * Integration tests for home schedule REST functionalities
 */
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class HomeScheduleRestControllerIT{

    @Autowired
    private MockMvc mvc;

    /**
     * get all the duties in a month
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void getDutiesInMonth() throws Exception {
        createDB();

        RequestBuilder request = get("/schedule/month/2022-04-01");
        MvcResult result=mvc.perform(request).andReturn();
        JSONArray json=new JSONArray(result.getResponse().getContentAsString());
        assertTrue(json.length() == 3);
    }

    /**
     * get all the duties in a week
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void getDutiesInWeek() throws Exception {
        createDB();

        RequestBuilder request = get("/schedule/week/2022-04-03");
        MvcResult result=mvc.perform(request).andReturn();
        JSONArray json=new JSONArray( result.getResponse().getContentAsString());
        assertTrue(json.length() == 2);
    }

    /**
     * get all the duties in a day
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void getDutiesInDay() throws Exception{
        createDB();

        RequestBuilder request = get("/schedule/day/2022-04-03");
        MvcResult result=mvc.perform(request).andReturn();
        JSONArray json=new JSONArray( result.getResponse().getContentAsString());
        assertTrue(json.length() == 1);
    }

    /**
     * get the detail of a specific duty
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void getDuty() throws Exception{
        createDB();

        RequestBuilder request = get("/schedule/5");
        MvcResult result=mvc.perform(request).andReturn();
        JSONObject json=new JSONObject(result.getResponse().getContentAsString());
        assertTrue((int) json.get("id") == 5);
    }

    /**
     * insert a new duty
     * @param date
     * @param ht
     */
    private void insertDuty(Date date, HomeTask ht){
        Duty d= new Duty(date, false, "n", ht, null);
        DutyRepositoryImpl.getInstance().insert(d);
    }

    /**
     * provide a simple DB to use it during the tests
     */
    private void createDB() {
        User u = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);

        Home h= new Home("MockHomeName", u,null);
        HomeRepositoryImpl.getInstance().insert(h);
        h.addUser(u);
        h.setId(1);

        HomeRepositoryImpl.getInstance().update(h);

        //UserRepositoryImpl.getInstance().update(u);

        User userDB = UserRepositoryImpl.getInstance().findById("a@a.com").get();

        Home home = HomeRepositoryImpl.getInstance().findById(1).get();



        HomeTask ht= new HomeTask("ht", h);
        HomeTaskRepositoryImpl.getInstance().insert(ht);


        insertDuty(new Date(122,3,2),ht);
        insertDuty(new Date(122,3,3),ht);
        insertDuty(new Date(122,3,4),ht);
    }
}