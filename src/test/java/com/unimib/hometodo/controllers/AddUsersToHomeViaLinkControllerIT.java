package com.unimib.hometodo.controllers;


import com.unimib.hometodo.config.WithMockCustomUser;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import com.unimib.hometodo.repositories.RegisterRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for joining a home via link functionalities
 */
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
class AddUsersToHomeViaLinkControllerIT {

    @Autowired
    private MockMvc mvc;

    User testUser;

    public AddUsersToHomeViaLinkControllerIT() {
        testUser = new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");
    }

    /**
     * Tests the login page correct load, in the join home via link functionality
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void joinViaLink() throws Exception{
        User u = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);


        String jsonHome = "{ \"name\" : \"home\", \"admin\" : \"a@a.com\", \"users\" : [\"a@a.com\"]}";


        this.mvc.perform(post("/home/add").content(jsonHome).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin("a@a.com");
        List<Home> userHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("a@a.com");

        assertEquals(1, adminHomes.size(), "The home should be associated to the user");
        assertEquals("a@a.com", adminHomes.get(0).getAdministrator().getEmail(), "The administrator should be the specified user");

        assertTrue(userHomes.size() > 0, "The home should be associated to the user");
        assertEquals("a@a.com", userHomes.get(0).getUsers().get(0).getEmail(), "The admin should also be in the user list");
        System.out.println(adminHomes.get(0).getJoinId());
        this.mvc.perform(get("/join-via-link/"+"a@a.com-"+adminHomes.get(0).getJoinId()).flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Tests the login functionality related to the join home via link functionality
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void login() throws Exception{
        User u = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);
        testUser.setPassword(RegisterRepositoryImpl.getInstance().calculateHmacString(testUser.getPassword()));
        UserRepositoryImpl.getInstance().insert(testUser);
        testUser.setPassword("Password!1");


        String jsonHome = "{ \"name\" : \"home\", \"admin\" : \"a@a.com\", \"users\" : [\"a@a.com\"]}";


        this.mvc.perform(post("/home/add").content(jsonHome).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin("a@a.com");
        List<Home> userHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("a@a.com");

        assertEquals(1, adminHomes.size(), "The home should be associated to the user");
        assertEquals("a@a.com", adminHomes.get(0).getAdministrator().getEmail(), "The administrator should be the specified user");

        assertTrue(userHomes.size() > 0, "The home should be associated to the user");
        assertEquals("a@a.com", userHomes.get(0).getUsers().get(0).getEmail(), "The admin should also be in the user list");


        System.out.println(adminHomes.get(0).getJoinId());
        this.mvc.perform(post("/join-via-link/"+"a@a.com-"+adminHomes.get(0).getJoinId()).flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().is(302));

        List<Home> homeWithNewUser = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(testUser.getEmail());
        assertTrue(!homeWithNewUser.isEmpty());
    }

    /**
     * test a fake join link to ensure that the user don't get added to a random home and that the security check prevents a malicious user to fake the link
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void login_wrong() throws Exception{
        User u = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);
        testUser.setPassword(RegisterRepositoryImpl.getInstance().calculateHmacString(testUser.getPassword()));
        UserRepositoryImpl.getInstance().insert(testUser);
        testUser.setPassword("Password!1");


        String jsonHome = "{ \"name\" : \"home\", \"admin\" : \"a@a.com\", \"users\" : [\"a@a.com\"]}";


        this.mvc.perform(post("/home/add").content(jsonHome).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin("a@a.com");
        List<Home> userHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("a@a.com");

        assertEquals(1, adminHomes.size(), "The home should be associated to the user");
        assertEquals("a@a.com", adminHomes.get(0).getAdministrator().getEmail(), "The administrator should be the specified user");

        assertTrue(userHomes.size() > 0, "The home should be associated to the user");
        assertEquals("a@a.com", userHomes.get(0).getUsers().get(0).getEmail(), "The admin should also be in the user list");


        System.out.println(adminHomes.get(0).getJoinId());
        this.mvc.perform(post("/join-via-link/"+"a@a.com-"+adminHomes.get(0).getJoinId()+"prr").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().is(200));

        List<Home> homeWithNewUser = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(testUser.getEmail());
        assertTrue(homeWithNewUser.isEmpty());
    }

    /**
     * test the correct load of the register webpage in the join home via link functionality
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void singUpPage() throws Exception{
        User u = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);


        String jsonHome = "{ \"name\" : \"home\", \"admin\" : \"a@a.com\", \"users\" : [\"a@a.com\"]}";


        this.mvc.perform(post("/home/add").content(jsonHome).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin("a@a.com");
        List<Home> userHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("a@a.com");

        assertEquals(1, adminHomes.size(), "The home should be associated to the user");
        assertEquals("a@a.com", adminHomes.get(0).getAdministrator().getEmail(), "The administrator should be the specified user");

        assertTrue(userHomes.size() > 0, "The home should be associated to the user");
        assertEquals("a@a.com", userHomes.get(0).getUsers().get(0).getEmail(), "The admin should also be in the user list");

        this.mvc.perform(get("/join-via-link/"+"a@a.com-"+adminHomes.get(0).getJoinId()+"/register").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    /**
     * tests the register functionality for the join a home via link functionality
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void registerUser() throws Exception{
        User u = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);


        String jsonHome = "{ \"name\" : \"home\", \"admin\" : \"a@a.com\", \"users\" : [\"a@a.com\"]}";


        this.mvc.perform(post("/home/add").content(jsonHome).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin("a@a.com");
        List<Home> userHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("a@a.com");

        assertEquals(1, adminHomes.size(), "The home should be associated to the user");
        assertEquals("a@a.com", adminHomes.get(0).getAdministrator().getEmail(), "The administrator should be the specified user");

        assertTrue(userHomes.size() > 0, "The home should be associated to the user");
        assertEquals("a@a.com", userHomes.get(0).getUsers().get(0).getEmail(), "The admin should also be in the user list");

        this.mvc.perform(post("/join-via-link/"+"a@a.com-"+adminHomes.get(0).getJoinId()+"/register").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk());

       // List<Home> homeWithNewUser = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(testUser.getEmail());
        Assertions.assertTrue(UserRepositoryImpl.getInstance().findById(testUser.getEmail()).isPresent());
    }
}