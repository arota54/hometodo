package com.unimib.hometodo.controllers;

import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.RegisterRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;


/**
 * Integration tests for password reset functionalities
 */
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class PasswordResetControllerIT {

    @Autowired
    private MockMvc mvc;

    User testUser;

    public PasswordResetControllerIT() {
        testUser = new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");
    }

    /**
     * return the reset page
     * @throws Exception
     */
    @Test
    void resetpage() throws Exception {
        this.mvc.perform(get("/resetPassword"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("User",new User()))
                .andExpect(model().attribute("status","insertEmail" ));
    }

    /**
     * check the email during the password reset
     * @throws Exception
     */
    @Test
    void checkEmail() throws Exception{
        this.mvc.perform(post("/resetPassword").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("status","userNotfound" ));
    }

    /**
     * check the email during the password reset
     * @throws Exception
     */
    @Test
    void checkEmail2() throws Exception{
        RegisterRepositoryImpl.getInstance().register(testUser);
        testUser.setPassword("Password!1");
        this.mvc.perform(post("/resetPassword").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("status","userFound" ));
    }


    /**
     * change the password
     * @throws Exception
     */
    @Test
    void changePassword() throws Exception{

        RegisterRepositoryImpl.getInstance().register(testUser);
        testUser.setPassword("Password!1");
        this.mvc.perform(post("/resetPassword/securityQuestion").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("status","passwordChanged" ));
    }

    /**
     * don't change the password because there are errors
     * @throws Exception
     */
    @Test
    void changePassword2() throws Exception{

        RegisterRepositoryImpl.getInstance().register(testUser);
        testUser.setPassword("Password!1");
        testUser.setSecurity_question_answer("rispostasbagliata");

        this.mvc.perform(post("/resetPassword/securityQuestion").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("status","securityCheckError" ));
    }
}