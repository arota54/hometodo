package com.unimib.hometodo.controllers;

import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.RegisterRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for login functionalities
 */
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class LoginControllerIT {
    @Autowired
    private MockMvc mvc;

    User testUser;

    public LoginControllerIT() {
        testUser = new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");
    }


    /**
     * return the login page
     * @throws Exception
     */
    @Test
    void loginPage() throws Exception{
        this.mvc.perform(get("/login"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("User",new User()));
    }

    /**
     * do the login correctly
     * @throws Exception
     */
    @Test
    void doLoginCorrectly() throws Exception{
        testUser= new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");

        RegisterRepositoryImpl.getInstance().register(testUser);
        Assertions.assertTrue(UserRepositoryImpl.getInstance().findById(testUser.getEmail()).isPresent());
        this.mvc.perform(post("/login").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * don't do the login because the credentials are wrong
     * @throws Exception
     */
    @Test
    void doLoginWithWrongCredentials() throws Exception{
        testUser= new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");

        this.mvc.perform(post("/login").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("status","wrong-credentials" ));

    }
}