package com.unimib.hometodo.controllers;

import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.RegisterRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Integration tests for register functionalities
 */
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class RegisterControllerIT {
    @Autowired
    private MockMvc mvc;

    User testUser;

    public RegisterControllerIT() {
        testUser = new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");
    }

    /**
     * do the registration
     * @throws Exception
     */
    @Test
    void singUpPage() throws Exception{
        this.mvc.perform(get("/register"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("User",new User()));
    }

    /**
     * check what happens with a normal registration
     * @throws Exception
     */
    @Test
    void registerUser() throws Exception{
        this.mvc.perform(post("/register").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("status","correctRegistration" ));

        Assertions.assertTrue(UserRepositoryImpl.getInstance().findById(testUser.getEmail()).isPresent());

    }

    /**
     * check if a user can't login with an existing user email
     * @throws Exception
     */
    @Test
    void registerUser2() throws Exception{
        RegisterRepositoryImpl.getInstance().register(testUser);
        testUser.setPassword("Password!1");
        this.mvc.perform(post("/register").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("status","error" ));

    }

    /**
     * check if a user can't login due to bad password
     * @throws Exception
     */
    @Test
    void registerUser3() throws Exception{
        testUser.setPassword("asd!1");
        this.mvc.perform(post("/register").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(model().attribute("status","error" ));

    }
}