package com.unimib.hometodo.controllers;


import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.RegisterRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for logout functionalities
 */
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class LogoutIT {
    @Autowired
    private MockMvc mvc;

    User testUser;

    public LogoutIT() {
        testUser = new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");
    }



    /**
     * do the login correctly, then logout correctly
     * @throws Exception
     */
    @Test
    void logout() throws Exception{
        testUser= new User();
        testUser.setName("davide");
        testUser.setSurname("piovani");
        testUser.setPassword("Password!1");
        testUser.setSecurity_question("1");
        testUser.setSecurity_question_answer("pollo");
        testUser.setEmail("piovanid@gmail.com");
        testUser.setHash("testHash");


        RegisterRepositoryImpl.getInstance().register(testUser);
        Assertions.assertTrue(UserRepositoryImpl.getInstance().findById(testUser.getEmail()).isPresent());

        this.mvc.perform(post("/login").flashAttr("user",testUser ))
                .andExpect(MockMvcResultMatchers.status().isOk());
        this.mvc.perform(post("/logout"))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(cookie().value("user_cookie", (String) null));

    }

}