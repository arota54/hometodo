package com.unimib.hometodo.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.assertj.core.api.Assertions;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for navbar functionalities
 */
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class NavbarControllerIT {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepositoryImpl userRepository;

    @Autowired
    private ObjectMapper mapper;


    /**
     * Test with a user in a home
     * @throws Exception
     */
    @Test
    public void testNavbarControllerUserInAHome() throws Exception {

        User u = new User("emailesempio@gmail.com", "Andrea", "Mariotti",
                "psw", null,
                null,
                "hash123");
        Home home = new Home("NomeCasa1");
        home.setAdministrator(u);
        home.addUser(u);

        // Insert a user
        UserRepositoryImpl.getInstance().insert(u);

        // Insert a home
        HomeRepositoryImpl.getInstance().insert(home);



        MvcResult result = (MvcResult) mvc.perform(MockMvcRequestBuilders.post("/navbar")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"email\" : \"" + u.getEmail() + "\", \"hash\" : \"" + u.getHash() +"\"}"))
                .andDo(print()).andExpect(status().isOk()).andReturn();


        Assertions.assertThat(result).isNotNull();
        String userJson = result.getResponse().getContentAsString();
        Assertions.assertThat(userJson).isNotEmpty();
        Assertions.assertThat(userJson).isEqualToIgnoringCase("{\"is_uh\":1}");
    }


    /**
     * Test with a user with no home
     * @throws Exception
     */
    @Test
    public void testNavbarControllerUserNotInAHome() throws Exception {

        User u = new User("emailesempio@gmail.com", "Andrea", "Mariotti",
                "psw", null,
                null,
                "hash1234");

        // Insert a user
        UserRepositoryImpl.getInstance().insert(u);

        MvcResult result = (MvcResult) mvc.perform(MockMvcRequestBuilders.post("/navbar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\" : \"" + u.getEmail() + "\", \"hash\" : \"" + u.getHash() +"\"}"))
                .andDo(print()).andExpect(status().isOk()).andReturn();


        Assertions.assertThat(result).isNotNull();
        String userJson = result.getResponse().getContentAsString();
        Assertions.assertThat(userJson).isNotEmpty();
        Assertions.assertThat(userJson).isEqualToIgnoringCase("{\"is_uh\":-1}");
    }


    /**
     * Test with a user in a home but with the wrong hash
     * @throws Exception
     */
    @Test
    public void testNavbarControllerUserNotInAHomeWithWrongHash() throws Exception {
        User u = new User("emailesempio@gmail.com", "Andrea", "Mariotti",
                "psw", null,
                null,
                "hash1234");

        // Insert a user
        UserRepositoryImpl.getInstance().insert(u);

        // Test 1 with a long wrong hash
        MvcResult result = (MvcResult) mvc.perform(MockMvcRequestBuilders.post("/navbar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\" : \"" + u.getEmail() + "\", \"hash\" : \"hashsbagliato\"}"))
                .andDo(print()).andExpect(status().isOk()).andReturn();

        Assertions.assertThat(result).isNotNull();
        String userJson = result.getResponse().getContentAsString();
        Assertions.assertThat(userJson).isNotEmpty();
        Assertions.assertThat(userJson).isEqualToIgnoringCase("{\"is_uh\":-1}");

        // Test 2 with a wrong hash set to null
        result = (MvcResult) mvc.perform(MockMvcRequestBuilders.post("/navbar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\" : \"" + u.getEmail() + "\", \"hash\" : \"\"}"))
                .andDo(print()).andExpect(status().isOk()).andReturn();


        Assertions.assertThat(result).isNotNull();
        userJson = result.getResponse().getContentAsString();
        Assertions.assertThat(userJson).isNotEmpty();
        Assertions.assertThat(userJson).isEqualToIgnoringCase("{\"is_uh\":-1}");

        // Test 3 with a wrong hash set only with numbers
        result = (MvcResult) mvc.perform(MockMvcRequestBuilders.post("/navbar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\" : \"" + u.getEmail() + "\", \"hash\" : \"1234\"}"))
                .andDo(print()).andExpect(status().isOk()).andReturn();


        Assertions.assertThat(result).isNotNull();
        userJson = result.getResponse().getContentAsString();
        Assertions.assertThat(userJson).isNotEmpty();
        Assertions.assertThat(userJson).isEqualToIgnoringCase("{\"is_uh\":-1}");
    }


    /**
     * Test with a user with the wrong email
     * @throws Exception
     */
    @Test
    public void testNavbarControllerWrongEmail() throws Exception {

        User u = new User("emailesempio1@gmail.com", "psw", "hash1234",
                "Andrea", "Mariotti", null, null);

        // Insert a user
        UserRepositoryImpl.getInstance().insert(u);

        // Test 1 wrong email right hash
        MvcResult result = (MvcResult) mvc.perform(MockMvcRequestBuilders.post("/navbar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\" : \"" + "" + "\", \"hash\" : \"" + u.getHash() +"\"}"))
                .andDo(print()).andExpect(status().isOk()).andReturn();


        Assertions.assertThat(result).isNotNull();
        String userJson = result.getResponse().getContentAsString();
        Assertions.assertThat(userJson).isNotEmpty();
        Assertions.assertThat(userJson).isEqualToIgnoringCase("{\"is_uh\":-1}");

        // Test 2 wrong email wrong hash
        result = (MvcResult) mvc.perform(MockMvcRequestBuilders.post("/navbar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"email\" : \"" + "" + "\", \"hash\" : \"" + "" +"\"}"))
                .andDo(print()).andExpect(status().isOk()).andReturn();


        Assertions.assertThat(result).isNotNull();
        userJson = result.getResponse().getContentAsString();
        Assertions.assertThat(userJson).isNotEmpty();
        Assertions.assertThat(userJson).isEqualToIgnoringCase("{\"is_uh\":-1}");
    }
}
