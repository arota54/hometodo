package com.unimib.hometodo.controllers;

import com.unimib.hometodo.config.WithMockCustomUser;
import com.unimib.hometodo.config.WithMockCustomUserInAHome;
import com.unimib.hometodo.models.Duty;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.HomeTask;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.DutyRepositoryImpl;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import com.unimib.hometodo.repositories.HomeTaskRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for home REST functionalities
 */
@SpringBootTest
@AutoConfigureMockMvc
@EnableWebMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class HomeRestControllerIT {

    @Autowired
    private MockMvc mvc;

    /**
     * add a new home
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void addHome() throws Exception {
        User u = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);


        String jsonHome = "{ \"name\" : \"home\", \"admin\" : \"a@a.com\", \"users\" : [\"a@a.com\"]}";


        this.mvc.perform(post("/home/add").content(jsonHome).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin("a@a.com");
        List<Home> userHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("a@a.com");

        assertEquals(1, adminHomes.size(), "The home should be associated to the user");
        assertEquals("a@a.com", adminHomes.get(0).getAdministrator().getEmail(), "The administrator should be the specified user");

        assertTrue(userHomes.size() > 0, "The home should be associated to the user");
        assertEquals("a@a.com", userHomes.get(0).getUsers().get(0).getEmail(), "The admin should also be in the user list");
    }
    /**
     * Check if the admin cannot add other homes with different admins
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void addHomeAnotherAdmin() throws Exception {

        User u2 = new User("b@b.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "b@b.com-mockHash");

        UserRepositoryImpl.getInstance().insert(u2);

        String jsonHome = "{ \"name\" : \"home\", \"admin\" : \"b@b.com\", \"users\" : [\"b@b.com\"]}";

        this.mvc.perform(post("/home/add").content(jsonHome).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin("b@b.com");
        List<Home> userHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("b@b.com");


        assertEquals(0, adminHomes.size(), "The home should not associated to the user");

        assertEquals(0,userHomes.size(), "The home should not be associated to the user");
    }
    /**
     * Check if the admin cannot add other homes that belong to it.
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void addMoreHomes() throws Exception {

        User u1 = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");

        UserRepositoryImpl.getInstance().insert(u1);

        String jsonHome = "{ \"name\" : \"home\", \"admin\" : \"a@a.com\", \"users\" : [\"a@a.com\"]}";

        this.mvc.perform(post("/home/add").content(jsonHome).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        this.mvc.perform(post("/home/add").content(jsonHome).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin("a@a.com");
        List<Home> userHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser("a@a.com");


        assertEquals(1, adminHomes.size(), "The admin should only own one home");
        assertEquals(1, userHomes.size(), "The user should only belong to one home");
        assertEquals(adminHomes.get(0), userHomes.get(0), "The admin should be associated as a user to the same home");


    }

    /**
     * get all the HomeTask in a home
     * @throws Exception
     */
    @Test
    @WithMockCustomUserInAHome
    void getHomeTasksInAHome() throws Exception {
        createDB();

        RequestBuilder request = get("/home/hometasks");
        MvcResult result = mvc.perform(request).andReturn();
        JSONArray json = new JSONArray(result.getResponse().getContentAsString());
        assertEquals(1, json.length());
    }

    /**
     * get all Users in a home
     * @throws Exception
     */
    @Test
    @WithMockCustomUserInAHome
    void getUsersInAHome() throws Exception {
        createDB();

        RequestBuilder request = get("/home/users");
        MvcResult result = mvc.perform(request).andReturn();
        JSONArray json = new JSONArray(result.getResponse().getContentAsString());
        assertEquals(1, json.length());
    }

    /**
     * Checks if user is deleted when admin makes the request and the user to delete is not an admin
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void deleteUserIfAdminRequestAndUserNoAdmin() throws Exception {

        createTwoUsersDB();

        RequestBuilder request = delete("/home/users/b@b.com/remove");
        MvcResult result = mvc.perform(request).andReturn();
        String resultString = result.getResponse().getContentAsString();
        assertEquals(resultString, "deleted", "Deleted action should be performed!");

        Optional<User> optUser = UserRepositoryImpl.getInstance().getUserInAHome("b@b.com", 1);
        assertFalse(optUser.isPresent(), "User should not belong to the homr");

    }

    /**
     * Checks if user is not deleted when home admin doesn't make the request
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void deleteUserIfNoAdminRequest() throws Exception {

        createTwoUsersDB();

        Home home = HomeRepositoryImpl.getInstance().findById(1).get();
        User notLoggedUsr = new User("c@c.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "c@c.com-mockHash");
        home.setAdministrator(notLoggedUsr);
        UserRepositoryImpl.getInstance().insert(notLoggedUsr);
        HomeRepositoryImpl.getInstance().update(home);

        RequestBuilder request = delete("/home/users/b@b.com/remove");
        MvcResult result = mvc.perform(request).andReturn();
        String resultString = result.getResponse().getContentAsString();
        assertEquals(resultString, "not deleted", "Deleted action should not be performed!");

        Optional<User> optUser = UserRepositoryImpl.getInstance().getUserInAHome("b@b.com", 1);
        assertTrue(optUser.isPresent(), "User should still belong to the home");

    }

    /**
     * Checks if user is not deleted when home admin makes the request and user to delete is the admin itself
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void deleteUserIfAdminRequestAndUserIsAdmin() throws Exception {

        createTwoUsersDB();

        RequestBuilder request = delete("/home/users/a@a.com/remove");
        MvcResult result = mvc.perform(request).andReturn();
        String resultString = result.getResponse().getContentAsString();
        assertEquals(resultString, "not deleted", "Deleted action should not be performed!");

        Optional<User> optUser = UserRepositoryImpl.getInstance().getUserInAHome("a@a.com", 1);
        assertTrue(optUser.isPresent(), "User should still belong to the home");

    }

    /**
     * provide a simple DB to use it during the tests
     */
    private void createDB() {
        User u = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);

        Home h= new Home("MockHomeName", u,null);
        HomeRepositoryImpl.getInstance().insert(h);
        h.addUser(u);
        h.setId(1);

        HomeRepositoryImpl.getInstance().update(h);

        HomeTask ht= new HomeTask("ht", h);
        HomeTaskRepositoryImpl.getInstance().insert(ht);
    }

    /**
     * Provides a simple DB with two ysers in a Home (one of them is the Home administrator) to use it during the tests
     */
    private void createTwoUsersDB() {
        User u1 = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        User u2 = new User("b@b.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "b@b.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u1);
        UserRepositoryImpl.getInstance().insert(u2);

        Home h= new Home("MockHomeName", u1,null);
        HomeRepositoryImpl.getInstance().insert(h);
        h.addUser(u1);
        h.addUser(u2);
        h.setId(1);

        HomeRepositoryImpl.getInstance().update(h);
    }


}