package com.unimib.hometodo.controllers;

import com.unimib.hometodo.config.WithMockCustomUser;
import com.unimib.hometodo.models.*;
import com.unimib.hometodo.repositories.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Integration tests for duty REST functionalities
 */
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class DutyRestControllerIT {

    @Autowired
    private MockMvc mvc;

    /**
     * edit a Duty already on the db
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void editDuty() throws Exception {
        createDB();

        Duty editDuty = DutyRepositoryImpl.getInstance().findById(3).get();
        editDuty.setDone(true);
        String jsonDuty = createDutyString(editDuty);

        this.mvc.perform(post("/duty/3/edit").content(jsonDuty).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        Duty duty = DutyRepositoryImpl.getInstance().findById(3).get();
        assertEquals(duty.isDone(), true);
    }

    /**
     * provide a simple DB to use it during the tests
     */
    private void createDB() {
        User a = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(a);

        User u = new User("u@u.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "u@u.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);
        List<User> users = new ArrayList<>();
        users.add(UserRepositoryImpl.getInstance().findById("u@u.com").get());

        Home h= new Home("MockHomeName", a,null);
        h.addUser(a);
        h.addUser(u);
        HomeRepositoryImpl.getInstance().insert(h);
        h.setId(1);

        HomeTask ht= new HomeTask("ht", h);
        HomeTaskRepositoryImpl.getInstance().insert(ht);

        Duty d = new Duty(new Date(122,3,2), false, "n", ht, null);
        DutyRepositoryImpl.getInstance().insert(d);
        d.setUsers(users);
        DutyRepositoryImpl.getInstance().update(d);

    }

    /**
     * support function for create the string of an array of duty
     * @throws JSONException
     */
    private String createDutyString(Duty duty) throws JSONException {
        JSONObject json = new JSONObject();

        json.put("id", duty.getId());
        json.put("date", duty.getDate());
        json.put("note", duty.getNote());
        json.put("done",duty.isDone());
        json.put("homeTask", duty.getHomeTask().getId());

        JSONArray jsonUsers = new JSONArray();
        for (User user : duty.getUsers()) {
            String s = user.getEmail();
            jsonUsers.put(s);
        }
        json.put("users", jsonUsers);
        return json.toString();
    }

    /**
     * add a new Duty with an existing HomeTask without Users
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void addDutyWithExistingHomeTaskWithoutUsers() throws Exception {
        HomeTask homeTaskDb = createDBWithHomeTask();

        String jsonDuty = createDutyString("2022-05-19 10:00:00", "", String.valueOf(homeTaskDb.getId()), homeTaskDb.getName(), homeTaskDb.getHome().getId(), false);

        this.mvc.perform(post("/duty/add").content(jsonDuty).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Duty> duties = (List<Duty>) DutyRepositoryImpl.getInstance().findAll();
        assertEquals(1, duties.size());
        assertEquals(0, duties.get(0).getUsers().size());
    }

    /**
     * add a new Duty without an existing HomeTask, so it has to be created without Users
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void addDutyWithoutExistingHomeTaskWithoutUsers() throws Exception {
        Home home = createDBWithoutHomeTask();

        String jsonDuty = createDutyString("2022-05-19 10:00:00", "", "", "ht", home.getId(), false);

        this.mvc.perform(post("/duty/add").content(jsonDuty).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Duty> duties = (List<Duty>) DutyRepositoryImpl.getInstance().findAll();
        assertEquals(1, duties.size());
        assertEquals(0, duties.get(0).getUsers().size());

        List<HomeTask> homeTasks = (List<HomeTask>) HomeTaskRepositoryImpl.getInstance().findAll();
        assertEquals(1, homeTasks.size());

        assertEquals("ht", homeTasks.get(0).getName());
        assertEquals(home.getId(), homeTasks.get(0).getHome().getId());
    }

    /**
     * add a new Duty with an existing HomeTask with Users
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void addDutyWithExistingHomeTaskWithUsers() throws Exception {
        HomeTask homeTaskDb = createDBWithHomeTask();

        String jsonDuty = createDutyString("2022-05-19 10:00:00", "", String.valueOf(homeTaskDb.getId()), homeTaskDb.getName(), homeTaskDb.getHome().getId(), true);

        this.mvc.perform(post("/duty/add").content(jsonDuty).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Duty> duties = (List<Duty>) DutyRepositoryImpl.getInstance().findAll();
        assertEquals(1, duties.size());
        assertEquals(1, duties.get(0).getUsers().size());
    }

    /**
     * add a new Duty without an existing HomeTask, so it has to be created with Users
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void addDutyWithoutExistingHomeTaskWithUsers() throws Exception {
        Home home = createDBWithoutHomeTask();

        String jsonDuty = createDutyString("2022-05-19 10:00:00", "", "", "ht", home.getId(), true);

        this.mvc.perform(post("/duty/add").content(jsonDuty).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());

        List<Duty> duties = (List<Duty>) DutyRepositoryImpl.getInstance().findAll();
        assertEquals(1, duties.size());
        assertEquals(1, duties.get(0).getUsers().size());



        List<HomeTask> homeTasks = (List<HomeTask>) HomeTaskRepositoryImpl.getInstance().findAll();
        assertEquals(1, homeTasks.size());

        assertEquals("ht", homeTasks.get(0).getName());
        assertEquals(home.getId(), homeTasks.get(0).getHome().getId());

    }

    private String createDutyString(String date, String note, String homeTaskId, String homeTaskName, int homeId, boolean user) throws JSONException {
        JSONObject json = new JSONObject();
        JSONObject jsonHomeTask = new JSONObject();
        jsonHomeTask.put("id", homeTaskId);
        jsonHomeTask.put("name", homeTaskName);
        jsonHomeTask.put("homeId", homeId);

        json.put("date", date);
        json.put("note", note);
        json.put("homeTask", jsonHomeTask);
        json.put("users", user);
        return json.toString();
    }

    /**
     * provide a simple DB with a HomeTask to use it during the tests
     */
    private HomeTask createDBWithHomeTask() {
        User u = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);

        Home h = new Home("MockHomeName", u,null);
        HomeRepositoryImpl.getInstance().insert(h);
        h.addUser(u);
        h.setId(1);

        HomeRepositoryImpl.getInstance().update(h);

        HomeTask ht = new HomeTask("ht", h);
        HomeTaskRepositoryImpl.getInstance().insert(ht);

        return ht;
    }

    /**
     * provide a simple DB without a HomeTask to use it during the tests
     */
    private Home createDBWithoutHomeTask() {
        User u = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(u);

        Home h = new Home("MockHomeName", u,null);
        HomeRepositoryImpl.getInstance().insert(h);
        h.addUser(u);
        h.setId(1);

        HomeRepositoryImpl.getInstance().update(h);

        return h;
    }
}
