package com.unimib.hometodo.controllers;

import com.unimib.hometodo.config.WithMockCustomUser;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;


import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for home functionalities
 */
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class HomeControllerIT {
    @Autowired
    private MockMvc mvc;

    /**
     * check if return correctly the form to add a new home
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void getHomeFormAdd_loggedUserInAHome() throws Exception{
        User mockUser = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(mockUser);
        Home mockHome = new Home();
        mockHome.setId(0);
        mockHome.setUsers(new ArrayList<>());
        mockHome.addUser(mockUser);
        HomeRepositoryImpl.getInstance().insert(mockHome);

        this.mvc.perform(get("/home/add"))
                .andExpect(view().name("redirect:/home"))
                .andExpect(redirectedUrl("/home?redirect_reason=belongs_to_home"));
    }
}