package com.unimib.hometodo.controllers;

import com.unimib.hometodo.config.WithMockCustomUser;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for Add user to home REST functionalities
 */
@SpringBootTest
@AutoConfigureMockMvc
@EnableWebMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class AddUserToHomeRESTIT {
    @Autowired
    private MockMvc mvc;

    /**
     * Add a new user to a home whose email doesn't exist
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void addNewUserToHomeWithANotExistingEmail() throws Exception {
        User u1 = new User();
        Home home = new Home();

        u1.setEmail("a@a.com");
        u1.setPassword("mockPassword1!");

        UserRepositoryImpl.getInstance().insert(u1);

        home.setAdministrator(u1);
        HomeRepositoryImpl.getInstance().insert(home);

        UserRepositoryImpl.getInstance().insert(u1);

        String jsonUserToAdd = "{ \"emailUser\" : \"andrea_da_inserire@gmail.com\"}";

        this.mvc.perform(post("/add/user-to-home").content(jsonUserToAdd).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk()).andExpect(content().json("{\"user_inserted\": -2}")); //email of the inserted user doesn't exist

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin(u1.getEmail());

        assertEquals(1, adminHomes.size(), "The home should be associated to the administrator user and stop.");
    }


    /**
     * Add a new user to a home whose email doesn't exist
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void addNewUserToHomeAlreadyUH() throws Exception {
        User u1 = new User();
        User admin2 = new User();
        User UH = new User();
        Home home = new Home();
        Home homeUH = new Home();

        admin2.setEmail("admin2@ciao.com");
        admin2.setPassword("mockPassword1!");
        UserRepositoryImpl.getInstance().insert(admin2);
        homeUH.setAdministrator(admin2);

        u1.setEmail("a@a.com");
        u1.setPassword("mockPassword1!");
        UserRepositoryImpl.getInstance().insert(u1);

        UH.setEmail("andrea_da_inserire@gmail.com");
        UH.setPassword("mockPassword2!");
        UserRepositoryImpl.getInstance().insert(UH);
        homeUH.setAdministrator(u1);
        homeUH.addUser(UH);

        HomeRepositoryImpl.getInstance().insert(homeUH);
        HomeRepositoryImpl.getInstance().insert(home);

        String jsonUserToAdd = "{ \"emailUser\" : \"andrea_da_inserire@gmail.com\"}";

        this.mvc.perform(post("/add/user-to-home").content(jsonUserToAdd).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk()).andExpect(content().json("{\"user_inserted\": -1}")); //email of the inserted user doesn't exist

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin(u1.getEmail());

        assertEquals(1, adminHomes.size(), "The home should be associated to the administrator user and stop because the other user we are trying to add is in another home.");
    }
}
