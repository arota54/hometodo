package com.unimib.hometodo.controllers;

import com.unimib.hometodo.config.WithMockCustomUser;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Integration tests for Add user to home get service
 */
@SpringBootTest
@AutoConfigureMockMvc
@EnableWebMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class AddUserToHomeIT {
    @Autowired
    private MockMvc mvc;

    /**
     * Check if return correctly the form to add a new user when the administrator is trying to get it
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void getUserFormAddToAHome_loggedUserInAHome() throws Exception{
        User mockUser = new User("a@a.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(mockUser);
        Home mockHome = new Home();
        mockHome.setAdministrator(mockUser);
        HomeRepositoryImpl.getInstance().insert(mockHome);

        this.mvc.perform(get("/add/user-in-a-home"))
                .andExpect(view().name("/home/add-user-to-home"));
    }


    /**
     * Check if return correctly the form to add a new user when a user who isn't the administrator is trying to get it
     * @throws Exception
     */
    @Test
    @WithMockCustomUser
    void getUserFormAddToAHome_NotAdministratorUser() throws Exception{
        User mockUser = new User("finto@fake.com", "mockName", "mockSurname",
                "mockPassword1!", "1", "mockQuestAns", "a@a.com-mockHash");
        UserRepositoryImpl.getInstance().insert(mockUser);
        Home mockHome = new Home();
        mockHome.setAdministrator(mockUser);
        HomeRepositoryImpl.getInstance().insert(mockHome);

        this.mvc.perform(get("/add/user-in-a-home"))
                .andExpect(view().name("redirect:/home"));
    }
}
