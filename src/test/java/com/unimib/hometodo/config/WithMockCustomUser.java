package com.unimib.hometodo.config;

import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * interface to provide a custom user
 */
@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockCustomUserSecurityContextFactory.class)
public @interface WithMockCustomUser {
    String email() default "a@a.com";

    String name() default "mockName";

    String surname() default "mockSurname";

    String password() default "mockPassword1!";

    String security_question() default "1";

    String security_question_answer() default "mockQuestAns";

    String hash() default "a@a.com-mockHash";
}
