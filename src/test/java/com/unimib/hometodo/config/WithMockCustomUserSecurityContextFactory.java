package com.unimib.hometodo.config;

import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.util.Collections;

/**
 * class to create a custom user to use it in the tests
 */
final class WithMockCustomUserSecurityContextFactory
        implements WithSecurityContextFactory<WithMockCustomUser> {

    /**
     * create a custom user
     * @param customUser custom user
     * @return SecurityContext
     */
    @Override
    public SecurityContext createSecurityContext(WithMockCustomUser customUser) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();


        User principal =
                new User(customUser.email(),
                         customUser.name(),
                        customUser.surname(),
                        customUser.password(),
                        customUser.security_question(),
                        customUser.security_question_answer(),
                        customUser.hash());
        Home home = new Home("MockHomeName",principal,null);
        home.setId(1);

        home.setAdministrator(principal);
        home.addUser(principal);

        Authentication auth =
                new UsernamePasswordAuthenticationToken(principal, null, Collections.emptyList());
        context.setAuthentication(auth);
        return context;
    }
}