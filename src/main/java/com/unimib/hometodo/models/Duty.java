package com.unimib.hometodo.models;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Duty {
    @Id
    @GeneratedValue
    private int id;
    private Date date;
    private boolean done;
    private String note;
    @ManyToOne
    @JoinColumn(name="home_task_id", nullable=false)
    private HomeTask homeTask;
    @ManyToMany
    @JoinTable(name = "duty_users")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<User> users;

    public Duty() {
    }

    public Duty(Date date, boolean done, String note, HomeTask homeTask, List<User> users) {
        this.date = date;
        this.done = done;
        this.note = note;
        this.homeTask = homeTask;
        this.users = users;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public HomeTask getHomeTask() {
        return homeTask;
    }

    public void setHomeTask(HomeTask homeTask) {
        this.homeTask = homeTask;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void deleteUser(User user) {
        this.users.remove(user);
    }

    @Override
    public String toString() {
        return "{" +
            "id:" + id +
            ", date:'" + date.toString() + "'"+
            ", done:" + done +
            ", note:'" + note + '\'' +
            ", homeTask:" + homeTask.toString() +
            ", users:"  + users.toString() +
            '}';
    }
}
