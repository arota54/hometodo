package com.unimib.hometodo.models;

import javax.persistence.*;

@Entity
@Table(name="Home_Task")
public class HomeTask {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    @ManyToOne()
    @JoinColumn(name="home_id", nullable=false)
    private Home home;

    public HomeTask() {
    }

    public HomeTask(String name, Home home) {
        this.name = name;
        this.home = home;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

    @Override
    public String toString() {
        return "{" +
                "id:" + id +
                ", name:'" + name + '\'' +
                ", home:" + home.toString() +
                '}';
    }
}
