package com.unimib.hometodo.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Users")
public class User {
    @Id
    @Column(unique = true)
    private String email;
    private String password;
    private String name;
    private String surname;
    private String hash;
    private String security_question;
    private String security_question_answer;

    public String getSecurity_question() {
        return security_question;
    }

    public void setSecurity_question(String security_question) {
        this.security_question = security_question;
    }

    public String getSecurity_question_answer() {
        return security_question_answer;
    }

    public void setSecurity_question_answer(String security_question_answer) {
        this.security_question_answer = security_question_answer;
    }

    public User(String email, String password, String name, String surname, String hash) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.hash = hash;

    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getEmail(), user.getEmail()) && Objects.equals(getPassword(), user.getPassword())
                && Objects.equals(getName(), user.getName()) && Objects.equals(getSurname(), user.getSurname())
                && Objects.equals(getHash(), user.getHash()) && Objects.equals(getSecurity_question(), user.getSecurity_question())
                && Objects.equals(getSecurity_question_answer(), user.getSecurity_question_answer());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail(), getPassword(), getName(), getSurname(), getHash(), getSecurity_question(), getSecurity_question_answer());
    }

    public User() {
        
    }

    public User(String email, String name, String surname, String password, String security_question, String security_question_answer,  String hash) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.hash = hash;
        this.security_question = security_question;
        this.security_question_answer = security_question_answer;
    }

    @Override
    public String toString() {
        return "{" +
                "email:'" + email + '\'' +
                ", password:'" + password + '\'' +
                ", name:'" + name + '\'' +
                ", surname:'" + surname + '\'' +
                ", hash:'" + hash + '\'' +
                ", securityQNumber:'" + security_question + '\'' +
                ", securityQanswer:'" + security_question_answer + '\'' +
                '}';
    }
}

