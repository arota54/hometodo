package com.unimib.hometodo.models;

import com.unimib.hometodo.repositories.RegisterRepositoryImpl;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Home {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;
    private String name;
    @OneToOne
    @JoinColumn(name="administrator_email")
    private User administrator;
    @OneToMany(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<User> users = new ArrayList<>();

    public Home(String homename){
        name = homename;
        users = null;
        administrator = null;
    }

    public Home(String name, User administrator, List<User> users) {
        this.name = name;
        this.administrator = administrator;
        this.users = users;
    }


    public Home() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getAdministrator() {
        return administrator;
    }

    public void setAdministrator(User administrator) {
        this.administrator = administrator;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addUser(User user) {
        if(users == null)
            users = new ArrayList<>();
        this.users.add(user);
    }

    public void deleteUser(User user) {
        this.users.remove(user);
    }


    @Override
    public String toString() {
        return "{" +
                "id:" + id +
                ", name:'" + name + '\'' +
                ", administrator:" + administrator.toString() +
                ", users:" + users.toString() +
        '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Home home = (Home) o;

        //da fare bene con la lista degli users
        return getId() == home.getId()
                && Objects.equals(getName(), home.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getAdministrator(), getUsers());
    }

    public String getJoinId(){
        return RegisterRepositoryImpl.getInstance().calculateHmacString(id+getAdministrator().getEmail());
    }

}
