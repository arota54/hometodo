package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Duty;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.HomeTask;

import java.util.List;

/**
 * Interface for Duty queries
 */
public interface DutyRepository extends Repository<Duty, Integer> {
    Iterable<Duty> findByDateRange(String start, String end, Iterable<HomeTask> hts);
    Iterable<Duty> findByDate(String day, Iterable<HomeTask> hts);
    public void detach(Duty duty);
    List countDutiesByHomeTask(String email);
    List findByUser(String email);
}
