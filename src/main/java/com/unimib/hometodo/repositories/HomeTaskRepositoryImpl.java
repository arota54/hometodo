package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Duty;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.HomeTask;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of the HomeRepository for queries about home tasks
 */
@Service
public class HomeTaskRepositoryImpl implements HomeTaskRepository {
    private static HomeTaskRepositoryImpl instance;
    private EntityManagerFactory entityManagerFactory;

    public HomeTaskRepositoryImpl() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.hometodo");
    }

    /**
     * Gets an Instance of HomeTaskRepositoryImpl
     * @return an instance of HomeTaskRepositoryImpl
     */
    public static HomeTaskRepositoryImpl getInstance() {
        if(instance == null)
            instance = new HomeTaskRepositoryImpl();
        return instance;
    }


    /**
     * Finds HomeTask by id
     * @param id
     * @return HomeTask with the specified id or a null HomeTask
     */
    @Override
    public Optional<HomeTask> findById(Integer id) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        HomeTask homeTask = entityManager.find(HomeTask.class, id);
        entityManager.close();
        return Optional.ofNullable(homeTask);
    }

    /**
     * Finds all home task in the database
     * @return an iterable containing all home tasks
     */
    @Override
    public Iterable<HomeTask> findAll() {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<HomeTask> hts = entityManager.createQuery("FROM HomeTask", HomeTask.class).getResultList();
        entityManager.close();
        return hts;
    }

    /**
     * Inserts a home task in the database
     * @param ht the home task to insert
     */
    @Override
    public void insert(HomeTask ht) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(ht);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void delete(HomeTask object) {

    }

    /**
     * Update a home task in the database
     * @param ht the home task to update
     */
    @Override
    public void update(HomeTask ht) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(ht);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    /**
     * Detaches a home task
     * @param hometask
     */
    @Override
    public void detach(HomeTask hometask) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        entityManager.detach(hometask);
    }

    /**
     * Finds all home tasks in a home
     * @param home
     * @return an Iterable of the home tasks in a home
     */
    public Iterable<HomeTask> findByHome(Home home) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<HomeTask> hts = entityManager.createQuery("SELECT ht FROM HomeTask ht INNER JOIN ht.home h WHERE h.id = :id", HomeTask.class).setParameter("id", home.getId()).getResultList();
        entityManager.close();
        return hts;
    }
}
