package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.HomeTask;

/**
 * Interface for HomeTask queries
 */
public interface HomeTaskRepository extends Repository<HomeTask, Integer> {
    public Iterable<HomeTask> findByHome(Home home);
    public void detach(HomeTask hometask);
}

