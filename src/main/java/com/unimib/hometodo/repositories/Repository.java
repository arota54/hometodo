package com.unimib.hometodo.repositories;

import java.util.Optional;

/**
 * Repository interface
 * @param <T> object
 * @param <I> id
 */
public interface Repository<T, I> {
    Optional<T> findById(I id);
    Iterable<T> findAll();
    void insert(T object);
    void delete(T object);
    void update(T object);
}
