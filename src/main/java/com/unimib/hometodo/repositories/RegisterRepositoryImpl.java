package com.unimib.hometodo.repositories;


import com.unimib.hometodo.models.User;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.text.html.Option;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of the RegisterRepository for queries about registration
 */
@Service
public class RegisterRepositoryImpl implements RegisterRepository  {
    private static RegisterRepositoryImpl instance;
    private EntityManagerFactory entityManagerFactory;


    public RegisterRepositoryImpl() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.hometodo");

    }
    /**
     * Gets an Instance of RegisterRepositoryImpl
     * @return an instance of RegisterRepositoryImpl
     */
    public static RegisterRepositoryImpl getInstance() {
        if(instance == null)
            instance = new RegisterRepositoryImpl();
        return instance;
    }

    /**
     * Hashes the passed string using HmacSHA512 adn returns the hash
     * @param string
     * @return String
     */
    public String calculateHmacString(String string) {
        byte[] secretKeyBytes = Objects.requireNonNull("unapiccolappendicelunedi").getBytes(StandardCharsets.UTF_8);

        byte[] valueBytes = Objects.requireNonNull(string).getBytes(StandardCharsets.UTF_8);

        try {
            Mac mac = Mac.getInstance("HmacSHA512");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKeyBytes, "HmacSHA512");
            mac.init(secretKeySpec);
            byte[] hmacBytes = mac.doFinal(valueBytes);
            return Base64.getEncoder().encodeToString(hmacBytes).replace("/","");

        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Hashes email and password of the passed user using HmacSHA512 adn returns the hash
     * @param user
     * @return String
     */
    private String calculateHmacUser(User user) {
        byte[] secretKeyBytes = Objects.requireNonNull("unapiccolappendicelunedi").getBytes(StandardCharsets.UTF_8);

        byte[] valueBytes = Objects.requireNonNull(user.getEmail() + "&" + user.getPassword()).getBytes(StandardCharsets.UTF_8);

        try {
            Mac mac = Mac.getInstance("HmacSHA512");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKeyBytes, "HmacSHA512");
            mac.init(secretKeySpec);
            byte[] hmacBytes = mac.doFinal(valueBytes);
            return Base64.getEncoder().encodeToString(hmacBytes);

        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Checks if a user is already present in the database
     * @param user
     * @return boolean
     */
    public boolean isEmailAlreadyInUse(User user){

        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<User> existingUser = null;
        existingUser = entityManager.createQuery("FROM User WHERE email = :email", User.class).setParameter("email", user.getEmail()).getResultList();

        if(!existingUser.isEmpty()){

            return true;

        }

        return false;
    }

    /**
     * Inserts the User in the database with a hashed password, returns true if the insertion was successful, false otherwise
     * @param user
     * @return boolean
     */
    public boolean register(User user){
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();

        if(!isEmailAlreadyInUse(user) && respectsPasswordConstraint(user.getPassword())) {
            user.setPassword(calculateHmacString(user.getPassword()));
            entityManager.getTransaction().begin();
            entityManager.persist(user);
            entityManager.getTransaction().commit();
            entityManager.close();
            return true;
        }else{
            return false;
        }
    }

    /**
     * Checks if the passed string matches the requirements of the password, 5+ characters, an uppercase letter, a lowercase letter, a number, a symbol
     * @param psw
     * @return boolean
     */
    public boolean respectsPasswordConstraint(String psw){
        Pattern pattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;'.,?/*~$^+=<>]).{5,100}$");
        Matcher matcher = pattern.matcher(psw);
        return matcher.find();
    }

    /**
     * changes the password of a user on the database by checking the security password
     * returns true if the change of password was successful, false otherwise
     * @param user
     * @return boolean
     */
    public boolean changePassword(User user){
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();

        if(isEmailAlreadyInUse(user) && respectsPasswordConstraint(user.getPassword())) {
            Optional<User> optionalUser = UserRepositoryImpl.getInstance().findById(user.getEmail());
            User tempUser = null;
            if (optionalUser.isPresent()) {
                tempUser = optionalUser.get();
                tempUser.setPassword(calculateHmacString(user.getPassword()));

                user = tempUser;
                entityManager.getTransaction().begin();
                entityManager.merge(user);
                entityManager.getTransaction().commit();
                entityManager.close();
                return true;
            }
        }else{
            return false;
        }

        return false;
    }
}
