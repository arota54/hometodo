package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;

/**
 * Interface for Home queries
 */
public interface HomeRepository extends Repository<Home, Integer> {
    Iterable<Home> findByUser(String email);
    Iterable<Home> findByAdmin(String email);
    public void detach(Home home);
    Iterable<User> findUsersByHome(int id);
}
