package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.User;

import java.util.Optional;

/**
 * Interface for User queries
 */
public interface UserRepository extends Repository<User, String> {
    public void detach(User user);

    Optional<User> getUserInAHome(String email, int id);
}
