package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Duty;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.HomeTask;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.*;

/**
 * Implementation of the DutyRepository for queries about duties
 */
@Service
public class DutyRepositoryImpl implements DutyRepository {
    private static DutyRepositoryImpl instance;
    private EntityManagerFactory entityManagerFactory;

    public DutyRepositoryImpl() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.hometodo");
    }

    /**
     * Gets an Instance of DutyRepositoryImpl
     * @return an instance of DutyRepositoryImpl
     */

    public static DutyRepositoryImpl getInstance() {
        if(instance == null)
            instance = new DutyRepositoryImpl();
        return instance;
    }


    @Override
    /**
     * Finds Duty by id
     * @param id
     * @return duty with the specified id or a null duty
     */
    public Optional<Duty> findById(Integer id) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        Duty duty =entityManager.find(Duty.class, id);
        entityManager.close();
        return Optional.ofNullable(duty);
    }

    /**
     * Detaches a duty
     * @param duty
     */
    @Override
    public void detach(Duty duty) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        entityManager.detach(duty);
    }

    /**
     * Finds all duties in the database
     * @return an iterable containing all duties
     */
    @Override
    public Iterable<Duty> findAll() {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<Duty> duties = entityManager.createQuery("FROM Duty", Duty.class).getResultList();
        entityManager.close();
        return duties;
    }

    /**
     * Inserts a duty in the database
     * @param duty
     */
    @Override
    public void insert(Duty duty) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(duty);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void delete(Duty object) {

    }

    /**
     * Modify the duty in the database
     * @param duty Duty
     */
    @Override
    public void update(Duty duty) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(duty);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    /**
     * Finds all duties in a date range by their home task id
     * @param start Start date
     * @param end End date
     * @param hts Home task ids
     * @return an Iterable of duties
     */
    @Override
    public Iterable<Duty> findByDateRange(String start, String end, Iterable<HomeTask> hts) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<Duty> duties = new ArrayList<>();
        for(HomeTask h:hts){
            duties.addAll(entityManager.createQuery("FROM Duty WHERE home_task_id=:id AND date BETWEEN '"+start+"' and '"+end+"'", Duty.class)
                    .setParameter("id",h.getId()).getResultList());
        }
        entityManager.close();
        return duties;
    }

    /**
     * Finds all duties in a specific date by their home task id
     * @param day The desired date
     * @param hts Home task ids
     * @return an Iterable of duties
     */
    @Override
    public Iterable<Duty> findByDate(String day, Iterable<HomeTask> hts) {
        String start = day+ " 00:00:00";
        String end = day+ " 23:59:59";
        return findByDateRange(start, end, hts);
    }

    @Override
    /**
     * Returns the home tasks associated to a user and their respective duty counts, based on the duties' status
     * @param email
     * @return a list of Objects[] of len 3. The Object contains the home task, the duties done value and the duties count
     */
    public List countDutiesByHomeTask(String email) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List result = entityManager.createQuery("SELECT d.homeTask, d.done, count(d) FROM Duty d LEFT JOIN d.users as u WHERE u.email = :email GROUP BY d.homeTask, d.done")
                .setParameter("email", email).getResultList();
        entityManager.close();
        return result;
    }

    @Override
    /**
     * Returns the duties of a specific User
     * @param email the email associated to the User
     * @return a List of Duty
     */
    public List findByUser(String email) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List result = entityManager.createQuery("SELECT d FROM Duty d LEFT JOIN d.users as u WHERE u.email = :email").setParameter("email", email).getResultList();
        entityManager.close();
        return result;
    }
}
