package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of the UserRepository for queries about users
 */
@Service
public class UserRepositoryImpl implements UserRepository {
    private static UserRepositoryImpl instance;
    private EntityManagerFactory entityManagerFactory;

    public UserRepositoryImpl() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.hometodo");
    }


    /**
     * Gets an Instance of UserRepositoryImpl
     * @return an instance of UserRepositoryImpl
     */
    public static UserRepositoryImpl getInstance() {
        if(instance == null)
            instance = new UserRepositoryImpl();
        return instance;
    }


    /**
     * Insert a User into db
     * @param user who we want to persist
     */
    @Override
    public void insert(User user) {
        if(user != null) {
            final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            if (entityManager.find(User.class, user.getEmail()) == null) {
                entityManager.persist(user);
                entityManager.getTransaction().commit();
            }
            entityManager.close();
        }
    }


    /**
     * gets the user from the database using the string that was contained into a cookie
     * @param principal
     * @return User
     */
    public  User GetUserByCookie(String principal) {
        String cookieEmail = principal.split("-",2)[0] ;
        String cookieHash = principal.split("-",2)[1] ;
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<User> existingUser = null;
        existingUser = entityManager.createQuery("FROM User WHERE email = :email AND hash = :hash", User.class)
                .setParameter("email", cookieEmail)
                .setParameter("hash",cookieHash)
                .getResultList();

        if(!existingUser.isEmpty()){
            return existingUser.get(0);
        }

        return null;
    }


    /**
     * Finds User by id
     * @param email
     * @return user with the specified id or a null user
     */
    @Override
    public Optional<User> findById(String email) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        User user = entityManager.find(User.class, email);
        entityManager.close();
        return Optional.ofNullable(user);
    }


    /**
     * Detaches a user
     * @param user
     */
    @Override
    public void detach(User user) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        entityManager.detach(user);
    }


    /**
     * Finds all users in the database
     * @return an iterable containing all users
     */
    @Override
    public Iterable<User> findAll() {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<User> users = entityManager.createQuery("FROM User", User.class).getResultList();
        entityManager.close();
        return users;
    }

    @Override
    public void delete(User object) {

    }


    /**
     * Updates a User already present in the database
     * @param user the user to update
     */
    @Override
    public void update(User user) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }


    /**
     * Checks if the security question is correct
     * @param user
     * @return boolean
     */
    public boolean checkSecurityPassword(User user) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        Optional<User> optionalUser = UserRepositoryImpl.getInstance().findById(user.getEmail());
        User dbUser = null;
        if (optionalUser.isPresent()) {
            dbUser = optionalUser.get();
        }

        if (dbUser.getSecurity_question().equals(user.getSecurity_question()) && dbUser.getSecurity_question_answer().equals(user.getSecurity_question_answer())) {
            return true;
        }

        return false;
    }


    /**
     * Get a User with a specific email and specific hash value who is registered in a home
     * @param email
     * @param hash alphanumeric field
     * @return User if the user hasn't the right hash value in the cookie (or the user hasn't a home or there isn't a user
     * with such email) returns a null User.
     */
    public User getUserInAHomeByEmailAndHash(String email, String hash){
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        User u = entityManager.find(User.class, email); //foundById(email)

        if(u != null){
            if(!u.getHash().equals(hash)) {
                // u has not the right hash, security problem
                return null;
            }
            else {
                List<Home> userHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(u.getEmail());
                boolean userHasAHome = userHouses.size() > 0;

                if(userHasAHome) {
                    // u has a home
                    return u;
                }
                else {
                    // u hasn't a home
                    return null;
                }
            }
        }
        else {
            // no one found
            return null;
        }
    }

    /**
     * Gets user if it belongs to a specific home
     * @param email the email of the user
     * @param id the id of the home
     * @return
     */

    @Override
    public Optional<User> getUserInAHome(String email, int id) {
        User userToGet = null;
        Optional<User> user = findById(email);
        if (user.isPresent()) {
            List<Home> homes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.get().getEmail());
            if (homes.size() > 0) {
                Home home = homes.get(0);
                if (home.getId() == id)
                    userToGet = user.get();
            }

        }
        Optional<User> userOptional = Optional.ofNullable(userToGet);
        return userOptional;
    }


    /**
     * Get a list of the users with no home.
     * @return an Iterable of User
     */
    public Iterable<User> getUsersWithNoHome(){
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<User> users = new ArrayList<>();
        List<Home> homes = new ArrayList<>();

        // Taking all the users
        users.addAll(entityManager.createQuery("SELECT u FROM User u", User.class).getResultList());

        // Taking all the users without a home
        homes.addAll(entityManager.createQuery("SELECT h FROM Home h", Home.class).getResultList());

        // Removing the users that are in the house from the users
        for(Home h : homes){
            for(User u : h.getUsers()){
                users.remove(u);
            }
        }

        entityManager.close();
        return users; // Users with no home
    }
}
