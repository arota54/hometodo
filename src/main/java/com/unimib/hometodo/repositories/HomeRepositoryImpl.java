package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.HomeTask;
import com.unimib.hometodo.models.User;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of the HomeRepository for queries about homes
 */
@Service
public class HomeRepositoryImpl implements HomeRepository {
    private static HomeRepositoryImpl instance;
    private EntityManagerFactory entityManagerFactory;


    public HomeRepositoryImpl() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.hometodo");
    }

    /**
     * Gets an Instance of DutyRepositoryImpl
     * @return an instance of DutyRepositoryImpl
     */
    public static HomeRepositoryImpl getInstance() {
        if(instance == null)
            instance = new HomeRepositoryImpl();
        return instance;
    }

    /**
     * Finds Home by id
     * @param id
     * @return home with the specified id or a null home
     */
    @Override
    public Optional<Home> findById(Integer id) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        Home home = entityManager.find(Home.class, id);
        entityManager.close();
        return Optional.ofNullable(home);
    }

    /**
     * Finds all homes in the database
     * @return an iterable containing all homes
     */
    @Override
    public Iterable<Home> findAll() {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<Home> homes = entityManager.createQuery("FROM Home", Home.class).getResultList();
        entityManager.close();
        return homes;
    }

    /**
     * Inserts a home in the database
     * @param home the home to insert
     */
    @Override
    public void insert(Home home) {
        if(home != null) {
            final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            if (entityManager.find(Home.class, home.getId()) == null) {
                entityManager.persist(home);
                entityManager.getTransaction().commit();
            }
            entityManager.close();
        }
    }

    /**
     * Detaches a home
     * @param home
     */
    @Override
    public void detach(Home home) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        entityManager.detach(home);
    }


    @Override
    public void delete(Home object) {

    }

    /**
     * Updates a home already present in the database
     * @param home the home to update
     */
    @Override
    public void update(Home home){
        if(home != null) {
            final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();
            if (entityManager.find(Home.class, home.getId()) != null) {
                entityManager.merge(home);
                entityManager.getTransaction().commit();
            }
            entityManager.close();
        }
    }

    /**
     * Finds a home with the specified admin id
     * @param email the id of the admin User
     * @return a Iterable of the Homes which belong to the admin
     */
    public Iterable<Home> findByAdmin(String email) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<Home> homes = entityManager.createQuery("SELECT h FROM Home h WHERE h.administrator.email = :email",
                Home.class).setParameter("email", email).getResultList();
        entityManager.close();
        return homes;
    }

    /**
     * Finds a home with the specified user id
     * @param email the id of the user
     * @return a Iterable of the Homes which belong to the user
     */
    public Iterable<Home> findByUser(String email) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<Home> homes = entityManager.createQuery("SELECT h FROM Home h LEFT JOIN h.users as u WHERE u.email = :email",
                Home.class).setParameter("email", email).getResultList();
        entityManager.close();
        return homes;
    }

    /**
     * Finds the list of users in a home
     * @param id the id of the home
     * @return a Iterable of the Homes which belong to the user
     */
    public Iterable<User> findUsersByHome(int id) {
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        Optional<Home> optionalHome = this.findById(id);
        Home home = null;
        if(optionalHome.isPresent())
            home = optionalHome.get();
        return  home.getUsers();
    }

    /**
     * Finds a home with the specified join_id
     * @param join_id the id used to join the home via link
     * @return a Iterable of the corresponding homes
     */
    public Iterable<Home> findByJoinId(String join_id) {
        String homeAdmin = join_id.split("-",2)[0] ;
        String homeHash = join_id.split("-",2)[1] ;
        return findByAdmin(homeAdmin);
    }

    public boolean checkJoinId(String join_id){
        String joinIdHash = join_id.split("-",2)[1] ;
        List<Home> home = (List<Home>) findByJoinId(join_id);
        if(!home.isEmpty()){
            String realHash = RegisterRepositoryImpl.getInstance().calculateHmacString(home.get(0).getId()+home.get(0).getAdministrator().getEmail());
            if(joinIdHash.equals(realHash)){
                return true;
            }
        }

        return false;
    }
}
