package com.unimib.hometodo.repositories;

import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.util.List;
import java.util.Optional;
import java.util.Random;

/**
 * Implementation of the LoginRepository for queries about logins
 */
@Service
public class LoginRepositoryImpl {
    private static LoginRepositoryImpl instance;
    private EntityManagerFactory entityManagerFactory;

    public LoginRepositoryImpl() {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("com.unimib.hometodo");
    }

    /**
     * Gets an Instance of LoginRepositoryImpl
     * @return an instance of LoginRepositoryImpl
     */
    public static LoginRepositoryImpl getInstance() {
        if(instance == null)
            instance = new LoginRepositoryImpl();
        return instance;
    }

    /**
     * Sets the hash on the Database for the user
     * @param user
     * @param hash
     */
    public void setHash(User user, String hash){
        Optional<User> optionalUserUser = UserRepositoryImpl.getInstance().findById(user.getEmail());
        User dbUser=null;

        if(optionalUserUser.isPresent()){
            dbUser = optionalUserUser.get();
        }else{
            return;
        }

        //user.setHome(dbUser.getHome());
        user.setName(dbUser.getName());
        user.setSurname(dbUser.getSurname());
        user.setSecurity_question_answer(dbUser.getSecurity_question_answer());
        user.setSecurity_question(dbUser.getSecurity_question());

        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        user.setHash(hash);

        user.setPassword(RegisterRepositoryImpl.getInstance().calculateHmacString(user.getPassword()));
        entityManager.getTransaction().begin();
        entityManager.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    /**
     * Check if on the database there is a user and check for password match on DB
     * returns true if a user exists and the password match, false otherwise
     * @param user
     * @return boolean
     */
    public boolean checkCredentials(User user){
        final EntityManager entityManager = this.entityManagerFactory.createEntityManager();
        List<User> existingUser = null;
        existingUser = entityManager.createQuery(" FROM User WHERE email = :email AND password = :password", User.class)
                .setParameter("email", user.getEmail())
                .setParameter("password",RegisterRepositoryImpl.getInstance().calculateHmacString( user.getPassword()))
                .getResultList();

        if(!existingUser.isEmpty()){
            return true;
        }
        return false;
    }

    /**
     * Creates the cookie string
     * @param user
     * @return String
     */
    public String createCookieString(User user){
            return user.getEmail()+"-"+getRandomString(128);
    }

    /**
     * generates a random string for the cookie
     * @return String
     */
    static String getRandomString(int n) {
        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }
}
