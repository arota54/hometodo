package com.unimib.hometodo.controllers;

import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * controller to return the schedule page
 */
@Controller
@RequestMapping("/schedule")
public class HomeScheduleController {

    /**
     * Returns the view showing the home schedule if the user belongs to a home
     * @param user authenticated user
     * @return String containing the url to redirect
     */
    @GetMapping(value = "")
    public String getSchedule(@AuthenticationPrincipal User user, Model model) {
        if (user != null){
            List<Home> userHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.getEmail());
            boolean isUser = userHouses.size() > 0;
            if (isUser) {
                return "schedule";
            } else {
                return "redirect:/";
            }
        } else {
            return "redirect:/login";
        }
    }
}
