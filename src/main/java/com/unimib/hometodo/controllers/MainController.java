package com.unimib.hometodo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * controller to return the main page
 */
@Controller
public class MainController {

    @GetMapping(value = "")
    public String getMainPage(Model model) {
        // static redirect to main page until there will be more pages
        model.addAttribute("message", "");
        return "mainpage";
    }
}
