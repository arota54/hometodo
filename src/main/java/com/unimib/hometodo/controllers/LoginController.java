package com.unimib.hometodo.controllers;

import com.unimib.hometodo.config.CookieAuthenticationFilter;
import com.unimib.hometodo.repositories.LoginRepositoryImpl;
import com.unimib.hometodo.repositories.RegisterRepository;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.unimib.hometodo.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.crypto.Mac;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

/**
 * controller to return login pages
 */
@Controller
@RequestMapping("/login")
public class LoginController {

    /**
     * Returns the view containing the form to login
     * @return ModelAndView
     */
    @GetMapping("")
    public ModelAndView loginPage() {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("User", new User());
        modelAndView.addObject("status","first-login");
        modelAndView.setViewName("login");

        return modelAndView;
    }

    /**
     * Either returns the login page with an error message if the login fails, or redirects to the main page
     * @param user authenticated user
     * @param response
     * @return ModelAndView
     */
    @PostMapping("")
    public ModelAndView  doLogin(@ModelAttribute("user") User user,
                                  HttpServletResponse response) {

        ModelAndView modelAndView = null;

        if (LoginRepositoryImpl.getInstance().checkCredentials(user)) {
            //Add the cookie hash to the user
            String hash = LoginRepositoryImpl.getInstance().createCookieString(user);
            LoginRepositoryImpl.getInstance().setHash(user,hash);

            //create the cookie
            String cookieValue = String.valueOf(user.getEmail()) + "-" +hash;
            Cookie cookie = new Cookie(CookieAuthenticationFilter.COOKIE_NAME, cookieValue);
            cookie.setPath("/");
            //  cookie.setMaxAge(Duration.of(1, ChronoUnit.DAYS).toSecondsPart());
            response.addCookie(cookie);
            modelAndView = new ModelAndView("redirect:/schedule");

        }else{
            modelAndView = new ModelAndView();
            modelAndView.addObject("User", new User());
            modelAndView.setViewName("login");
            modelAndView.addObject("status","wrong-credentials");
        }

        return modelAndView;
    }
}
