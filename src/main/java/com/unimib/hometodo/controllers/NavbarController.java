package com.unimib.hometodo.controllers;

import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller to return the navbar
 */
@RestController
public class NavbarController {
    /**
     * This REST service is needed by the navbar.js in order to know if the User is a User in a home or not (in order
     * to change the view of the menu on the navbar).
     *
     * @param user_param_request contains both the email and the hash passed with a json to the REST service
     * @return a json that indicates if a user is a user in a home (1) or not (-1)
     */
    @PostMapping(value = "/navbar", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public String isUserInAHome(@RequestBody User user_param_request) {

        User user_home = (User) UserRepositoryImpl.getInstance().getUserInAHomeByEmailAndHash(
                user_param_request.getEmail(),
                user_param_request.getHash());

        JSONObject jsonObject = new JSONObject();

        if(user_home != null) {
            // user has a home
            jsonObject.put("is_uh", 1);
        }else{
            // user hasn't a home or someone tried to manipulate the user_cookie or no one found with that email
            jsonObject.put("is_uh", -1);
        }

        return jsonObject.toString();
    }
}
