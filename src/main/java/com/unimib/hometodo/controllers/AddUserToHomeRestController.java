package com.unimib.hometodo.controllers;


import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Controller to Insert a User in a Home (REST Features)
 */
@RestController
public class AddUserToHomeRestController {

    /**
     * This function is used only to take a JSON and to understand if the given user is a UH or not, or if the email does
     * not exist.
     * @param json {"emailUser": "email@example.com"}
     * @return null if the email doesn't exist or if the user with that email is already in a home
     */
    public User parseUserJson(String json) {
        JSONObject jsonObject = new JSONObject(json);
        String userToAddToHomeEmail = jsonObject.get("emailUser").toString();

        Optional<User> user = UserRepositoryImpl.getInstance().findById(userToAddToHomeEmail);

        String allEmailsWithNoHome = getAllEmailsWithNoHome();

        if (user.isPresent())
            // Checking if the email is owned by a user with no home
            if(allEmailsWithNoHome.contains(user.get().getEmail()))
                return user.get();

        // There is no user with this email inside the db, or he has a home already
        return null;
    }

    /**
     * If the user can be added to the house, it's added.
     * @param json the user to add {"emailUser": "email@example.com"}
     * @param administrator the user who is trying to add a new user
     * @return a json to know if the user was inserted in the home or not with different coding:
     *         {"user_inserted": -2}: the email of the user we wanted to insert doesn't exist
     *         {"user_inserted": -1}: the user we wanted to insert has already a home
     *         {"user_inserted": 0}: the user the USER that is trying to add a new user is not an ADMIN
     *         {"user_inserted": 1}: the user has been inserted in the home
     */
    @PostMapping(value = "/add/user-to-home", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public String addUserToHome(@RequestBody String json, @AuthenticationPrincipal User administrator) {
        JSONObject jsonObject = new JSONObject();
        User user_to_add = parseUserJson(json);

        Home administratorHome = null;


        // if the user has no home
        if(user_to_add != null){
            // Take the home of the administrator
            Iterable<Home> homeToAddUser = HomeRepositoryImpl.getInstance().findByAdmin(administrator.getEmail());

            if(homeToAddUser != null){
                Iterator<Home> homeToAddUserIt = homeToAddUser.iterator();
                if(homeToAddUserIt.hasNext()){
                    // This is the home in which we'll add the new user.
                    administratorHome = homeToAddUserIt.next();
                }else{
                    jsonObject.put("user_inserted", 0);
                    return jsonObject.toString();
                }
            }else{
                jsonObject.put("user_inserted", 0);
                return jsonObject.toString();
            }

            // If all went ok
            if(administratorHome != null){
                // Adding the user entered in the form in the administrator's house
                administratorHome.addUser(user_to_add);
                // Update the house with the new user
                HomeRepositoryImpl.getInstance().update(administratorHome);
            }else{
                jsonObject.put("user_inserted", 0);
                return jsonObject.toString();
            }

            // User inserted correctly
            return jsonObject.put("user_inserted", 1).toString();

        }else{
            // User not inserted correctl
            String email_user_to_add = new JSONObject(json).get("emailUser").toString();
            Iterable<Home> homeUserToAdd = HomeRepositoryImpl.getInstance().findByUser(email_user_to_add);
            Iterator<Home> itHome = homeUserToAdd.iterator();

            // The user has already a house
            if(itHome.hasNext()){
                return jsonObject.put("user_inserted", -1).toString();
            }else{
                // The email doesn't exist
                return jsonObject.put("user_inserted", -2).toString();
            }

        }
    }


    /***
     * This REST is needed to provide all the users without a home in a search bar that auto completes itself when
     * you're writing.
     * @return a json object {"emails": "p@a.com,c@g.com,..,..,.."}
     */
    @PostMapping(value = "/add/user-in-a-home/get-all-users-with-no-home", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String getAllEmailsWithNoHome() {

        Iterable<User> usersWithNoHome = UserRepositoryImpl.getInstance().getUsersWithNoHome();

        // This is a string with all users emails split by comma
        String emails = "";

        // Taking all the emails
        Iterator<User> iter = usersWithNoHome.iterator();

        while(iter.hasNext()){
            emails = emails + iter.next().getEmail() +  ",";
        }

        // To avoid the problem of handle the last comma, I'll just not consider the last "end"
        emails = emails + "end";

        JSONObject jsonObject = new JSONObject();

        // If there is at least one email
        if(emails.length() > 3){
            emails = emails.substring(0, emails.length() - 4); // -4 cause it's the ",end"
        } else {
            emails = "";
        }

        jsonObject.put("emails", emails);

        return jsonObject.toString();
    }

}
