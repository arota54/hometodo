package com.unimib.hometodo.controllers;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.RegisterRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

/**
 * controller to provide the password reset
 */
@Controller
@RequestMapping("/resetPassword")
public class PasswordResetController {

    /**
     * Returns the view which asks for the email of the user of which the password needs to be reset
     * @param user authenticated user
     * @return ModelAndView
     */
    @GetMapping("")
    public ModelAndView resetpage(@ModelAttribute("user") User user) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("User", new User());
        modelAndView.addObject("status", "insertEmail");

        modelAndView.setViewName("resetPassword");
        return modelAndView;
    }

    /**
     * Returns the view which asks for the security question
     * collects all data to be passed to /securityQuestion
     * @param user authenticated user
     * @return ModelAndView
     */
    @PostMapping("")
    public ModelAndView checkEmail(@ModelAttribute("user") User user) {
        ModelAndView modelAndView = new ModelAndView();

            Optional<User> optionalUser = UserRepositoryImpl.getInstance().findById(user.getEmail());//
            if (optionalUser.isPresent()) {
                user = optionalUser.get();
                modelAndView = new ModelAndView("resetPassword");
                modelAndView.addObject("status", "userFound");
                user.setSecurity_question_answer(null);
                user.setPassword(null);
            } else {
                modelAndView.addObject("status", "userNotfound");
            }

        modelAndView.addObject("User", user);


        return modelAndView;
    }

    /**
     * Returns a view were is shown the result of the change of password
     * @param user authenticated user
     * @return ModelAndView
     */
    @PostMapping("/securityQuestion")
    public ModelAndView changePassword(@ModelAttribute("user") User user) {
        ModelAndView modelAndView= new ModelAndView();
        modelAndView.setViewName("resetPasswordSQ");
        //if the security question is right
        if( UserRepositoryImpl.getInstance().checkSecurityPassword(user) ) {

            //if the change of password is successful
            if (RegisterRepositoryImpl.getInstance().changePassword(user)) {
                modelAndView.addObject("status", "passwordChanged");//set the view to show that the password was successfully changed
            } else {
                modelAndView.addObject("status", "securityCheckError");//set the view to show that the password was not successfully changed
            }
        }else{
            modelAndView.addObject("status", "securityCheckError");
        }
        return modelAndView;
    }
}
