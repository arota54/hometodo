package com.unimib.hometodo.controllers;
import com.unimib.hometodo.repositories.RegisterRepositoryImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.unimib.hometodo.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * controller to return register pages
 */
@Controller
@RequestMapping("/register")
public class RegisterController {

    /**
     * Returns the view containing the register form
     * @param user authenticated user
     * @return ModelAndView
     */
    @GetMapping("")
    public ModelAndView singUpPage(@ModelAttribute User user) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("User", new User());
        modelAndView.addObject("status", "register");
        modelAndView.setViewName("register");

        return modelAndView;
    }

    /**
     * Returns the view containing the result of the registration of the user
     * @param user authenticated user
     * @return ModelAndView
     */
    @PostMapping("")
    public ModelAndView registerUser(@ModelAttribute("user") User user) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("User", new User());

        if(RegisterRepositoryImpl.getInstance().register(user)){
            modelAndView.addObject("status", "correctRegistration");
        }else{
            modelAndView.addObject("status", "error");

        }

        modelAndView.setViewName("register");

        return modelAndView;
    }
}
