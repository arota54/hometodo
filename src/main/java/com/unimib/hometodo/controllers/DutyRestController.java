package com.unimib.hometodo.controllers;

import com.unimib.hometodo.models.*;
import com.unimib.hometodo.repositories.*;
import org.json.*;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.annotation.AuthenticationPrincipal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * REST controller to handle Duty information
 */
@RestController
public class DutyRestController {

    /**
     * REST method that consumes a JSON containing the Duty to modify
     * @param json the String containing the information = (id, date, done, note, homeTask = id ,users = List of id)
     */
    @PostMapping(value="/duty/{id}/edit", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void editDuty(@RequestBody String json, @PathVariable int id) {
        JSONObject jsonObject = new JSONObject(json);
        Duty parsedDuty = parseJsonToDuty(jsonObject);
        parsedDuty.setId(id);
        DutyRepositoryImpl.getInstance().update(parsedDuty);
    }

    /**
     * method that parse the duty json into a duty object.
     * @param jsonObject: object that contains the json of a duty
     * @return Duty: object of the duty parsed
     */
    public Duty parseJsonToDuty(JSONObject jsonObject) {
        JSONArray usersId = jsonObject.getJSONArray("users");
        List<User> users = new ArrayList();
        String userId;
        Optional<User> user= null;

        for (int i = 0; i < usersId.length(); i++) {
            userId = String.valueOf(usersId.get(i));
            user = UserRepositoryImpl.getInstance().findById(userId);
            if (user.isPresent())
                users.add(user.get());
        }
        String dateString = jsonObject.getString("date");
        String note = jsonObject.getString("note");
        Boolean done = jsonObject.getBoolean("done");

        int jsonHomeTask = jsonObject.getInt("homeTask");
        Optional<HomeTask> homeTask = HomeTaskRepositoryImpl.getInstance().findById(jsonHomeTask);
        System.out.println(jsonHomeTask);
        System.out.println(HomeTaskRepositoryImpl.getInstance().findAll());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Duty(date, done, note, homeTask.get(), users);
    }

    /**
     * REST method that consumes a JSON containing the Duty to insert
     * @param json the String containing the information = (date, note*, homeTask = (id* (if it's new or not), name), users = true/false)
     * @param user authenticated user
     */
    @PostMapping(value = "/duty/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addDuty(@RequestBody String json, @AuthenticationPrincipal User user) {
        JSONObject jsonObject = new JSONObject(json);

        JSONObject jsonHomeTask = jsonObject.getJSONObject("homeTask");
        HomeTask homeTask = parseHomeTaskJson(jsonHomeTask, user);
        String dateString = jsonObject.getString("date");
        String note = jsonObject.getString("note");

        boolean booleanUser = jsonObject.getBoolean("users");
        List<User> users = new ArrayList<>();
        if(booleanUser) {
            Optional<User> optionalUser = UserRepositoryImpl.getInstance().findById(user.getEmail());
            optionalUser.ifPresent(users::add);
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = formatter.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Duty duty = new Duty(date, false, note, homeTask, users);

        DutyRepositoryImpl.getInstance().insert(duty);

        return;
    }

    /**
     * method that parse the HomeTask json into a HomeTask object.
     * @param jsonHomeTask the String containing the information = (id* (if it's new or not), name)
     * @param user to get the home
     * @return HomeTask object: an existing one (from the db) or a new one
     */
    public HomeTask parseHomeTaskJson(JSONObject jsonHomeTask, User user) {
        HomeTask homeTask = null;

        String homeTaskIdString = jsonHomeTask.getString("id");
        String homeTaskName = jsonHomeTask.getString("name");
        Home home = null;

        // homeTaskId = "" means that a new HomeTask has to be created
        if(homeTaskIdString.equals("")) {
            List<Home> homes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.getEmail());

            homeTask = new HomeTask(homeTaskName, homes.get(0));
            HomeTaskRepositoryImpl.getInstance().insert(homeTask);
        }
        // an existed HomeTask has been selected
        else {
            int homeTaskId = Integer.parseInt(homeTaskIdString);
            Optional<HomeTask> optionalHomeTask = HomeTaskRepositoryImpl.getInstance().findById(homeTaskId);

            if(optionalHomeTask.isPresent()) {
                homeTask = optionalHomeTask.get();
            }
        }

        return homeTask;
    }

}
