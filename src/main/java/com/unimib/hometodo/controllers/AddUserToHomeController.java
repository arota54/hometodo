package com.unimib.hometodo.controllers;

import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 *  Controller to add a new user to a home (NOT REST)
 */

@Controller
public class AddUserToHomeController {
    /**
     * Returns the view showing the home information if the user that is truing to access it is not an administrator.
     * Returns the view of the login if the user that is trying to access to this feature is not logged.
     * Return the view to add a user in a home in case the previous cases are not met.
     * @param user is the user that is trying to add a new user to his home
     * @return the right view
     */
    @GetMapping("/add/user-in-a-home")
    public ModelAndView getFormAddNewUserToHome(@AuthenticationPrincipal User user) {
        ModelAndView mv = new ModelAndView();
        if (user != null){

            // Search if the user is an admin of one house
            List<Home> userHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin(user.getEmail());

            boolean isAdministrator = userHouses.size() > 0;
            if (isAdministrator) {
                //mv.addObject("redirect_reason", "belongs_to_home");
                mv.setViewName("/home/add-user-to-home");
            } else {
                //mv.addObject("home", home);
                mv.setViewName("redirect:/home");
            }
        } else {
            mv.setViewName("redirect:/login");
        }
        return mv;
    }
}
