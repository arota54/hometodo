package com.unimib.hometodo.controllers;
import com.unimib.hometodo.models.*;
import com.unimib.hometodo.repositories.*;
import org.json.*;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import java.util.*;

/**
 * REST controller to retrieve home schedule information
 */
@RestController
public class HomeScheduleRestController {

    /**
     * REST method that collecting all the duties of a Home in a month
     * @param date
     * @param user authenticated user
     * @return JSON string
     */
    @GetMapping(value = "/schedule/month/{date}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String getDutiesInMonth(@PathVariable String date, @AuthenticationPrincipal User user) {
        String dateStr[]=date.split("-");
        int monthS = Integer.parseInt(dateStr[1])-2;
        int monthE = Integer.parseInt(dateStr[1])+1;
        int year =Integer.parseInt(dateStr[0])-1900;
        Date startD = new Date(year, monthS, 1);
        Date endD = new Date(year, monthE, 0);
        Iterable<HomeTask> temp = HomeTaskRepositoryImpl.getInstance().findAll();

        // user's home
        List<Home> userHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.getEmail());
        Home userHome = userHouses.get(0);

        Iterable<HomeTask> hts = HomeTaskRepositoryImpl.getInstance().findByHome(userHome);
        Iterable<Duty> duties = DutyRepositoryImpl.getInstance().findByDateRange(fromDateToString(startD), fromDateToString(endD), hts);

        JSONArray jsonDuties =new JSONArray();
        for (Duty d:duties) {
            JSONObject o = new JSONObject(d.toString());
            jsonDuties.put(o);
        }
        return jsonDuties.toString();
    }

    /**
     * REST method that collecting all the duties of a Home in a week
     * @param date
     * @param user authenticated user
     * @return JSON string
     */
    @GetMapping(value = "/schedule/week/{date}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String getDutiesInWeek(@PathVariable String date, @AuthenticationPrincipal User user) {
        String dateStr[]=date.split("-");
        int month = Integer.parseInt(dateStr[1])-1;
        int year =Integer.parseInt(dateStr[0])-1900;
        int day = Integer.parseInt(dateStr[2]);
        Date startD = new Date(year, month, day);
        Date endD = new Date(year, month, day+6);

        // user's home
        List<Home> userHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.getEmail());
        Home userHome = userHouses.get(0);

        Iterable<HomeTask> hts = HomeTaskRepositoryImpl.getInstance().findByHome(userHome);
        Iterable<Duty> duties = DutyRepositoryImpl.getInstance().findByDateRange(fromDateToString(startD), fromDateToString(endD), hts);
        JSONArray jsonDuties =new JSONArray();
        for (Duty d:duties) {
            JSONObject o = new JSONObject(d.toString());
            jsonDuties.put(o);
        }
        return jsonDuties.toString();
    }


    /**
     * REST method that collecting all the duties of a Home in a day
     * @param date
     * @param user authenticated user
     * @return JSON string
     */
    @GetMapping(value = "/schedule/day/{date}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String getDutiesInDay(@PathVariable String date, @AuthenticationPrincipal User user) {
        // user's home
        List<Home> userHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.getEmail());
        Home userHome = userHouses.get(0);

        Iterable<HomeTask> hts = HomeTaskRepositoryImpl.getInstance().findByHome(userHome);
        Iterable<Duty> duties = DutyRepositoryImpl.getInstance().findByDate(date, hts);
        JSONArray jsonDuties =new JSONArray();
        for (Duty d:duties) {
            JSONObject o = new JSONObject(d.toString());
            jsonDuties.put(o);
        }
        return jsonDuties.toString();
    }

    /**
     * REST method that collecting the detail of a specific duty
     * @param id id of the duty
     * @return JSON string
     */
    @GetMapping(value = "/schedule/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String getDuty(@PathVariable Integer id) {
        Optional<Duty> duty = DutyRepositoryImpl.getInstance().findById(id);
        if(!duty.isPresent()) {
            return "";
        }
        else{
            Duty d=duty.get();
            JSONObject jsonDuty = new JSONObject(d.toString());
            return jsonDuty.toString();
        }
    }

    /**
     * Parses a date into a String
     * @param d
     * @return String representing the date
     */
    private String fromDateToString(Date d){
        int year = d.getYear()+1900;
        int month = d.getMonth()+1;
        String strDate = year+"-"+month+"-"+d.getDate()+" 00:00:00";
        return strDate;
    }
}
