package com.unimib.hometodo.controllers;

import com.unimib.hometodo.models.Duty;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.HomeTask;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.DutyRepositoryImpl;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import com.unimib.hometodo.repositories.HomeTaskRepositoryImpl;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller to retrieve Home information
 */
@RestController
public class HomeRestController {

    /**
     * REST method that consumes a JSON containing the home name, the admin id and the user ids and inserts the corresponding home
     * @param json the String containing the information
     */
    @PostMapping(value = "/home/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addHome(@RequestBody String json, @AuthenticationPrincipal User user) {
        Home home_to_add = parseHomeJson(json);
        User admin = home_to_add.getAdministrator();

        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin(admin.getEmail());
        List<Home> userHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(admin.getEmail());

        if (home_to_add.getAdministrator().equals(user) && adminHomes.size() == 0 && userHomes.size() == 0) {
            HomeRepositoryImpl.getInstance().insert(home_to_add);
        }
        return;
    }

    /**
     * Parses a JSON String into a Home object
     * @param json String containing the information
     * @return Home object with the information provided
     */
    public Home parseHomeJson(String json) {
        JSONObject jsonObject = new JSONObject(json);
        JSONArray jsonUsers = jsonObject.getJSONArray("users");
        User admin = null;
        List<User> users = new ArrayList<>();
        String adminEmail = jsonObject.getString("admin");

        Optional<User> adminOptional = UserRepositoryImpl.getInstance().findById(adminEmail);
        if (adminOptional.isPresent())
            admin = adminOptional.get();

        for (int i = 0; i < jsonUsers.length(); i++) {
            String userEmail = (String) jsonUsers.get(i);
            Optional<User> user = UserRepositoryImpl.getInstance().findById(userEmail);
            if (user.isPresent())
                users.add(user.get());
        }

        String name = jsonObject.getString("name");
        Home home = new Home(name, admin, users);

        return home;
    }

    /**
     * Gets duty counts split by HomeTask for all users in a given Home
     * @param id the id of the Home
     * @param user the authenticated User
     * @return a JSON string of all information
     */
    @GetMapping(value="/home/{id}/taskcount", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String getHomeUserTaskCount(@PathVariable Integer id, @AuthenticationPrincipal User user) {
        Optional<Home> home = HomeRepositoryImpl.getInstance().findById(id);
        JSONArray jsonUserCount = new JSONArray();

        if (home.isPresent()) {
            for (User u : home.get().getUsers()) {
                JSONArray jsonCountByHomeTask = new JSONArray();
                List<Object[]> res = DutyRepositoryImpl.getInstance().countDutiesByHomeTask(user.getEmail());

                for (Object[] result : res) {
                    JSONObject res_object = new JSONObject();
                    res_object.put("home_task", ((HomeTask) result[0]).getName());
                    res_object.put("count", result[1]);
                    jsonCountByHomeTask.put(res_object);
                }

                JSONObject o = new JSONObject();
                o.put("user", u.getEmail());
                o.put("counts", jsonCountByHomeTask);

                jsonUserCount.put(o);
            }
            return jsonUserCount.toString();
        }
        return "";
    }


    /**
     * REST method that get all the HomeTasks in a Home
     * @param user authenticated user
     * @return JSON string
     */
    @GetMapping(value = "/home/hometasks", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String getHomeTasksInAHome(@AuthenticationPrincipal User user) {
        // user's home
        List<Home> userHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.getEmail());
        Home userHome = userHouses.get(0);

        Iterable<HomeTask> hts = HomeTaskRepositoryImpl.getInstance().findByHome(userHome);

        JSONArray jsonHomeTasks =new JSONArray();
        for (HomeTask ht:hts) {
            JSONObject o = new JSONObject(ht.toString());
            jsonHomeTasks.put(o);
        }
        return jsonHomeTasks.toString();
    }

    /**
     * REST method that get all the users in a Home
     * @param user authenticated user
     * @return JSON string
     */
    @GetMapping(value = "/home/users", produces = {MediaType.APPLICATION_JSON_VALUE})
    public String getUsersInAHome(@AuthenticationPrincipal User user) {
        // user's home
        List<Home> userHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.getEmail());
        Home userHome = userHouses.get(0);

        Iterable<User> users = HomeRepositoryImpl.getInstance().findUsersByHome(userHome.getId());

        JSONArray jsonUsers =new JSONArray();
        for (User u:users) {
            JSONObject o = new JSONObject(u.toString());
            jsonUsers.put(o);
        }
        return jsonUsers.toString();
    }

    /**
     * Removes a user with the specified id from its Home. Request must be started from the Home admin
     * @param id the id of the user to delete
     * @param user the current logged user
     * @return
     */
    @DeleteMapping(value="/home/users/{id}/remove")
    public String removeUserFromHome(@PathVariable String id, @AuthenticationPrincipal User user) {
        List<Home> adminHomes = (List<Home>) HomeRepositoryImpl.getInstance().findByAdmin(user.getEmail());

        // checks if the user is admin in a home
        if (adminHomes.size() > 0 && adminHomes.get(0).getAdministrator().getEmail().equals(user.getEmail())) {
            Home home = adminHomes.get(0);
            Optional<User> delUser = UserRepositoryImpl.getInstance().getUserInAHome(id, home.getId());
            // checks if the user is in the admin home and it isn't the administrator
            if (delUser.isPresent() && !(delUser.get().equals(home.getAdministrator()))) {
                List<Duty> userDuties = DutyRepositoryImpl.getInstance().findByUser(id);
                for (Duty d : userDuties) {
                    d.deleteUser(delUser.get());
                    DutyRepositoryImpl.getInstance().update(d);
                }
                home.getUsers().remove(delUser.get());
                HomeRepositoryImpl.getInstance().update(home);

                return "deleted";
            }
        }
        return "not deleted";
    }

}
