package com.unimib.hometodo.controllers;

import com.unimib.hometodo.models.Duty;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.models.HomeTask;
import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.DutyRepositoryImpl;
import com.unimib.hometodo.repositories.HomeRepositoryImpl;
import com.unimib.hometodo.repositories.HomeTaskRepository;
import com.unimib.hometodo.repositories.HomeTaskRepositoryImpl;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * controller to return Home pages
 */
@Controller
public class HomeController {

    /**
     * Returns the view showing the home information of the logged user
     * @param user authenticated user
     * @return ModelAndView
     */
    @GetMapping("/home")
    public ModelAndView getHome(@AuthenticationPrincipal User user) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("user", user);

        List<Home> userHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.getEmail());
        boolean isUser = userHouses.size() > 0;
        HashMap<User, List<Object[]>> countMap = new HashMap<>();

        if(isUser) {
            Home home = userHouses.get(0);
            Iterable<HomeTask> tasks = HomeTaskRepositoryImpl.getInstance().findByHome(home);

            for (User u : home.getUsers()) {
                List<Object[]> res = DutyRepositoryImpl.getInstance().countDutiesByHomeTask(u.getEmail());
                countMap.put(u, res);
            }

            mv.addObject("home", home);
            mv.addObject("countMap", countMap);
            mv.addObject("tasks", tasks);
            if (home.getAdministrator().equals(user))
                mv.addObject("isAdmin", home.getAdministrator().getEmail());

            //to add the join_id to the webpage if the user is the admin of the house
            if(user.getEmail().equals(userHouses.get(0).getAdministrator().getEmail())){
                mv.addObject("join_id","http://localhost:8080/join-via-link/"+user.getEmail()+"-"+userHouses.get(0).getJoinId());
            }
        }
        else {
            mv.addObject("home", null);
            mv.addObject("counts", null);
            mv.addObject("tasks", null);
            mv.addObject("isAdmin", null);
        }

        mv.setViewName("home/view-home");
        return mv;
    }

    /**
     * Returns the view containing the form to add a new home. Redirects accordingly to the user's status
     * @param user authenticated user
     * @return ModelAndView
     */
    @GetMapping("/home/add")
    public ModelAndView getHomeFormAdd(@AuthenticationPrincipal User user) {
        ModelAndView mv = new ModelAndView();
        if (user != null){
            List<Home> userHouses = (List<Home>) HomeRepositoryImpl.getInstance().findByUser(user.getEmail());
            boolean isUser = userHouses.size() > 0;
            if (isUser) {
                mv.addObject("redirect_reason", "belongs_to_home");
                mv.setViewName("redirect:/home");
            } else {
                Home home = new Home(null, user, null);
                mv.addObject("home", home);
                mv.setViewName("home/add-home");
            }
        } else {
            mv.setViewName("redirect:/login");
        }
        return mv;
    }

}
