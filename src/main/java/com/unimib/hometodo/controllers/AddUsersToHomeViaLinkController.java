package com.unimib.hometodo.controllers;

import com.unimib.hometodo.config.CookieAuthenticationFilter;
import com.unimib.hometodo.models.Home;
import com.unimib.hometodo.repositories.*;
import org.springframework.boot.Banner;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.unimib.hometodo.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.crypto.Mac;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

/**
 * controller to add a user to a home with a link
 */
@Controller
@RequestMapping("/join-via-link")
public class AddUsersToHomeViaLinkController {

    /**
     * This method shows the first login page let the user choose between loggin in right away or sign up
     * @param join_id The join-ID of the home, used to identify which home the user should be added to
     * @return The Login webpage
     */
    @GetMapping("/{join_id}")
    public ModelAndView joinViaLink(@PathVariable String join_id) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("User", new User());
        modelAndView.setViewName("login");


        List<Home> home = (List<Home>) HomeRepositoryImpl.getInstance().findByJoinId(join_id);

        String joinIdHash = join_id.split("-",2)[1] ;
        if (!home.isEmpty()&& home.get(0).getJoinId().equals(joinIdHash)){
            modelAndView.addObject("status","first-login");
            modelAndView.addObject("home",home.get(0));
            modelAndView.setViewName("addUsersToHomeViaLink");
            modelAndView.addObject("join_id", join_id);


        }else {
            modelAndView.addObject("status","no-home");
            modelAndView = new ModelAndView("redirect:/error");

        }


        return modelAndView;

    }

    /**
     * Maps the login functionality and redirects to the correct page, either error or the home page of the home who the user joined via the link
     * @param join_id The join-ID of the home, used to identify which home the user should be added to
     * @param user The user who's loggin in and that should be added the home
     * @param response Used for security checks and for the cookie
     * @return Either redirects to the home page or returns a ModelAndView of the login page with an error message
     */
    @PostMapping("/{join_id}")
    public ModelAndView login(@PathVariable String join_id,
                              @ModelAttribute("user") User user,
                              HttpServletResponse response) {

        ModelAndView modelAndView = null;
        List<Home> home_list = (List<Home>) HomeRepositoryImpl.getInstance().findByJoinId(join_id);
        Home home = home_list.get(0);


        if (LoginRepositoryImpl.getInstance().checkCredentials(user) &&
                !home_list.isEmpty() &&
                home_list.get(0).getJoinId().equals(join_id.split("-",2)[1]) ) {
            //Add the cookie hash to the user
            String hash = LoginRepositoryImpl.getInstance().createCookieString(user);
            LoginRepositoryImpl.getInstance().setHash(user,hash);
            user.setHash(hash);
            //create the cookie
            String cookieValue = String.valueOf(user.getEmail()) + "-" +hash;
            Cookie cookie = new Cookie(CookieAuthenticationFilter.COOKIE_NAME, cookieValue);
            cookie.setPath("/");
            //  cookie.setMaxAge(Duration.of(1, ChronoUnit.DAYS).toSecondsPart());
            response.addCookie(cookie);

            //add the user in the home
            boolean alreadyInHome = false;
          //  if( !home.getAdministrator().getEmail().equals(user.getEmail())) {
                List<Home> homes = (List<Home>)HomeRepositoryImpl.getInstance().findAll();
                for(Home h : homes) {
                    for (User u : h.getUsers()) {
                        if (u.getEmail().equals(user.getEmail()))
                            alreadyInHome = true;
                    }

                }
                if (!alreadyInHome) {
                    home.addUser(user);
                    HomeRepositoryImpl.getInstance().update(home);
                }
         //   }
            modelAndView = new ModelAndView("redirect:/home");
            if(alreadyInHome)
                modelAndView.addObject("alreadyInHome","true");


        }else{
            modelAndView = new ModelAndView();
            modelAndView.addObject("User", new User());
            modelAndView.setViewName("addUsersToHomeViaLink");
            modelAndView.addObject("status","wrong-credentials");
        }

        return modelAndView;

    }


    /**
     * Returns the view containing the register form, allows to keep the join id id as a parameter
     * @param join_id The join-ID of the home, used to identify which home the user should be added to
     * @param user Authenticated user
     * @return ModelAndView The register page
     */
    @GetMapping("/{join_id}/register")
    public ModelAndView singUpPage(@PathVariable String join_id,
                                   @ModelAttribute User user) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("User", new User());
        modelAndView.addObject("status", "register");
        modelAndView.addObject("join_id", join_id);
        modelAndView.setViewName("addUsersToHomeViaLinkRegister");

        return modelAndView;
    }

    /**
     * Returns the view containing the result of the registration of the user and sign up the user
     * @param join_id The join-ID of the home, used to identify which home the user should be added to
     * @param user Authenticated user
     * @return ModelAndView Either a page that redirects to the login or reload the same page with an error
     */
    @PostMapping("/{join_id}/register")
    public ModelAndView registerUser(@PathVariable String join_id,
                                     @ModelAttribute("user") User user) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("User", new User());
        modelAndView.addObject("join_id", join_id);

        if(RegisterRepositoryImpl.getInstance().register(user)){
            modelAndView.addObject("status", "correctRegistration");
        }else{
            modelAndView.addObject("status", "error");

        }

        modelAndView.setViewName("addUsersToHomeViaLinkRegister");

        return modelAndView;
    }

}
