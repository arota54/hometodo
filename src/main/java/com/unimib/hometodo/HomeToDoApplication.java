package com.unimib.hometodo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeToDoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HomeToDoApplication.class, args);
    }

}
