package com.unimib.hometodo.config;


import com.unimib.hometodo.models.User;
import com.unimib.hometodo.repositories.UserRepositoryImpl;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * This class encapsulates the behavior that the authentication provider should have depending on the type of authentication information that was present in the http request
 */
@Component
public class UserAuthProvider implements AuthenticationProvider {

    private final UserRepositoryImpl userRepository;

    public UserAuthProvider(UserRepositoryImpl userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * This method controls that the user that was present in the cookie is a registered user, and that the email and password are correct, then, it provides to the app the information
     * @param authentication the authentication token created by the CookieAuthenticationFilter class
     * @return an authentication token that can be used safely and that contains the logged user information
     * @throws AuthenticationException
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        User loggedUser =null;

        if(authentication instanceof PreAuthenticatedAuthenticationToken){
             loggedUser= userRepository.GetUserByCookie((String) authentication.getPrincipal());
        }

        if (loggedUser == null){

            return null;

        }

        return new UsernamePasswordAuthenticationToken(loggedUser, null, Collections.emptyList());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
