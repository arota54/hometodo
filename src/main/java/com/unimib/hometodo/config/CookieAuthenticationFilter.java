package com.unimib.hometodo.config;

import com.unimib.hometodo.repositories.RegisterRepositoryImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * class that contains method that are called in the authentication filter to manage the cookie present in a http request
 */
public class CookieAuthenticationFilter extends OncePerRequestFilter {

    public static final String COOKIE_NAME = "user_cookie";

    /**
     * This method checks for authentication cookies in the http request and sets up a security context which contains the authentication information
     * @param request the request that was intercepted
     * @param response the response that has to be passed forward to the filter chain
     * @param filterChain the filter cahin
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        Optional<Cookie> cookieAuth = Stream.of(Optional.ofNullable(request.getCookies()).orElse(new Cookie[0]))
                .filter(cookie -> COOKIE_NAME.equals(cookie.getName()))
                .findFirst();

        if (cookieAuth.isPresent()) {
            SecurityContextHolder.getContext().setAuthentication(
                    new PreAuthenticatedAuthenticationToken(cookieAuth.get().getValue(), null));
        }

        filterChain.doFilter(request, response);
    }
}
