package com.unimib.hometodo.config;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

/**
 * Class that contains all the security  configurations of the app
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    private final UserAuthenticationEntryPoint userAuthenticationEntryPoint;

    public SecurityConfig(UserAuthenticationEntryPoint userAuthenticationEntryPoint, UserAuthProvider userAuthProvider) {
        this.userAuthenticationEntryPoint = userAuthenticationEntryPoint;
    }

    /**
     * Method that configure all the security settings and every control that happens when a http/https request is intercepted
     * This configuration also include the configuration of the endpoint /logout which is automatically set up by spring and will handle the cookie deletion
     * @param http the http request that gets intercepted for security reasons
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
       http

               .exceptionHandling().authenticationEntryPoint(userAuthenticationEntryPoint)
               .and()
               .addFilterBefore(new CookieAuthenticationFilter(), BasicAuthenticationFilter.class)
               .csrf().disable()
               .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
               .and()
               .logout().deleteCookies(CookieAuthenticationFilter.COOKIE_NAME) //this line specify how to behave when a logout happens, and it makes spring delete the cookie
               .and()
               .authorizeRequests()
               .antMatchers(HttpMethod.GET, "/login","/login/logged", "/register/**","/resetPassword/**","/navbar/**","/join-via-link/**").permitAll()
               .antMatchers(HttpMethod.POST, "/login","/login/logged","/register/**","/resetPassword/**","/navbar/**","/join-via-link/**").permitAll()
               .antMatchers("/h2-console/**").permitAll()
               .anyRequest().authenticated();

        http.csrf().disable();
        http.headers().frameOptions().disable();
    }
}
