// extract the information within the form to create a json representing the duty
function createDuty(day) {
    // "yy-MM-dd hh:mm:ss"
    var date = day + " " + padTo2Digits($("#addDutyModalHour").val()) + ":00:00";
    var note = $("#addDutyModalNote").val();

    var user;
    if($("#addDutyModalUser").is(':checked'))
        user = true;
    else
        user = false;

    var newHomeTask = "";
    var selectedHomeTask = $("#addDutyModalSelectHomeTask").children("option:selected").val();
    if(selectedHomeTask === "no") {
        newHomeTask = $("#addDutyModalNewHomeTask").val();

        if(newHomeTask === "") {
            $("#addDutyModalError").modal('show');
            $("#addDutyModalErrorText").text('You have to insert a new HomeTask if you do not choose an existing one!');

        }
        else {

            $.ajax({
                url: "/home/hometasks",
                success: function (result) {
                    for (const homeTask of result) {
                        if (homeTask.name.toUpperCase() === newHomeTask.toUpperCase()) {
                            $("#addDutyModalError").modal('show');
                            $("#addDutyModalErrorText").text('HomeTask already exists!');
                            return false;
                        }
                    }

                    var homeTask = {
                        id: "",
                        name: newHomeTask,
                    }

                    var duty = {
                        date: date,
                        note: note,
                        homeTask: homeTask,
                        users: user,
                    }

                    addNewDuty(duty);
                }
            });
        }
    }
    else {
        var homeTask = {
            id: $("#addDutyModalSelectHomeTask").children("option:selected").attr('id'),
            name: selectedHomeTask,
        }

        var duty = {
            date: date,
            note: note,
            homeTask: homeTask,
            users: user,
        }

        addNewDuty(duty);
    }

    return false;
}

// create an ajax post request to add a new duty
function addNewDuty(duty) {
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/duty/add",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        data: JSON.stringify(duty),
        dataType: 'json',
        success: function (data) {
            var obj = data;
            $('#addDutyModal').modal('toggle');
            return false;
        },
        error: function (e) {
            $('#addDutyModal').modal('toggle');
            return false;
        }
    });
}

// prepare the form in the modal to add a new duty
function handleAddingNewDuty(date, hour, isMonthView) {
    $("#addDutyModal").modal('toggle');

    $("#addDutyModalDate").val(date);

    var day = parseInt(date.split("-")[2].replace(/^0+/, ""));
    var month = parseInt(date.split("-")[1].replace(/^0+/, "")) - 1;
    var year = parseInt(date.split("-")[0].replace(/^0+/, ""));
    var valHour;
    if(hour === "")
        valHour = new Date().getHours() + 1;
    else
        valHour = hour;

    $("#addDutyModalHour").val("");
    if(isToday(month, day, year)) {
        $("#addDutyModalHour").attr({
            "max" : 23,
            "min" : new Date().getHours() + 1
        });
        $("#addDutyModalHour").val(valHour);
    }
    else {
        $("#addDutyModalHour").attr({
            "max" : 23,
            "min" : 0
        });
        $("#addDutyModalHour").val(valHour);
    }
    if(isMonthView)
        $("#addDutyModalHour").prop("disabled", false);
    else
        $("#addDutyModalHour").prop("disabled", true);


    fillSelectWithHomeTasks();

    $("#addDutyModalSelectHomeTask").change(function () {
        var selectedHomeTask = $("#addDutyModalSelectHomeTask").children("option:selected").val();
        $("#addDutyModalNewHomeTask").val("");

        // enable adding a new HomeTask
        if(selectedHomeTask === "no") {
            $("#addDutyModalNewHomeTask").prop("disabled", false);
        }
        // disable adding a new HomeTask
        else {
            $("#addDutyModalNewHomeTask").prop("disabled", true);
        }
    });

    $("#addDutyButton").click(function() {
        createDuty(date);
        return false;
    });


    return false;
}

// fill the select with all the HomeTask in the Home
function fillSelectWithHomeTasks() {
    $("#addDutyModalSelectHomeTask option").remove();
    $("#addDutyModalSelectHomeTask").append(
        $("<option selected value></option>")
            .text("Nothing selected")
            .val("no")
    )

    $.ajax({
        url: "/home/hometasks",
        success: function(result){
            for(const homeTask of result){
                $("#addDutyModalSelectHomeTask").append(
                    $("<option></option>")
                        .text(homeTask.id + ": " + homeTask.name)
                        .val(homeTask.name)
                        .attr("id", homeTask.id)
                )
            }
        }
    });
}