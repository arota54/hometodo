function printInfoDutyInModal(duty) {
    var dayHour, hour, day;

    $("#editUsersDutyDiv input").remove()
    $("#editUsersDutyDiv label").remove()

    $("#dutyNameModal").html(duty.homeTask.name);

    // print the date and the time
    dayHour = duty.date.split('.')[0]   // "YY-MM-DD hh:mm:ss"
    dayHour = dayHour.substring(0, dayHour.length - 3)  // "YY-MM-DD hh:mm"
    day = dayHour.split(' ')[0]
    $("#editDateDuty").val(day.split('-')[2] + "/" + day.split('-')[1] + "/" + day.split('-')[0])
    $("#editDateDuty").prop('disabled', true);

    hour = dayHour.split(' ')[1].split(':')[0]
    $("#editTimeDuty").val(hour);
    $("#editTimeDuty").prop('disabled', true);

    $("#editNoteDuty").val(duty.note)
    $("#editNoteDuty").prop('disabled', true);

    // print if a duty is done or not
    if(duty.done) {
        $("#editDoneDuty").prop("checked", true)
    }
    $("#editDoneDuty").prop('disabled', true);

    // print the users assigned to the duty
    $.ajax({
        url: "/home/users",
        success: function(result){
            for(const userA of result){
                const userInput = createElementWithClassType("input", "form-check-input","checkbox");
                email = userA.email.replace("@","_").replace(".","-")
                userInput.setAttribute("id", email)
                const userLabel = createElementWithClass("label", "");
                userLabel.innerText = userA.name + " " + userA.surname;
                for(const user of duty.users) {
                    if(userA.email == user.email)
                        userInput.checked = "checked"
                }
                userInput.disabled = "disabled"
                $("#editUsersDutyDiv").append(userInput);
                $("#editUsersDutyDiv").append(userLabel);
            }
        }
    });
    $("#dutyDetailModal").modal('show');
    $("#saveEditDutyButton").hide();
    $("#editDutyButton").show();

    $("#editDutyButton").click(function() {
        $('.form-control').prop('disabled', false);
        user_cookie = readCookie("user_cookie")
        first_score = user_cookie.indexOf("-");
        email = user_cookie.substring(0, first_score).replace("@","_").replace(".","-")
        $('#'+email).prop('disabled', false);
        $("#editDoneDuty").prop('disabled', false);
        $("#saveEditDutyButton").show();
        $("#editDutyButton").hide();
    });

    $("#saveEditDutyButton").click(function() {
        if(dateFormat()){
            if(!dateModify(duty) || (dateModify(duty) && dateAfter())){
                if(checkedIfPast()){
                    $("#dutyDetailModal").modal('hide');
                    uploadDuty(duty)
                    location.reload();
                }
                else{
                    $("#addDutyModalError").modal('show');
                    $("#addDutyModalErrorText").text('You cannot check a duty that has not yet passed');
                    $('#editDoneDuty').prop("checked", false)
                }
            }
            else{
                $("#addDutyModalError").modal('show');
                $("#addDutyModalErrorText").text('You have to pick a date/time after now');
            }
        }
        else{
            $("#addDutyModalError").modal('show');
            $("#addDutyModalErrorText").text('Wrong date format, the format is: dd/mm/yyyy');
        }
    });
}

function checkedIfPast(){
    if($('#editDoneDuty').is(':checked')){
        if(dateAfter()){
            return false;
        }
        else{
            return true;
        }
    }
    else{
        return true
    }
}

function dateModify(duty){
    var parts = $("#editDateDuty").val().split("/");
    pickedDate = new Date()
    pickedDate.setFullYear(parseInt(parts[2], 10))
    pickedDate.setDate(parseInt(parts[0], 10))
    pickedDate.setMonth(parseInt(parts[1], 10)-1)
    pickedDate.setHours($("#editTimeDuty").val())

    dutyDate = new Date()
    partsDuty = duty.date.split(" ")
    dutyDate.setFullYear(partsDuty[0].split("-")[0])
    dutyDate.setMonth(parseInt(partsDuty[0].split("-")[1], 10)-1)
    dutyDate.setDate(partsDuty[0].split("-")[2])
    dutyDate.setHours(partsDuty[1].split(":")[0])

    if(dutyDate.valueOf() == pickedDate.valueOf())
        return false
    else
        return true
}

function dateAfter(){
    var parts = $("#editDateDuty").val().split("/");
    pickedDate = new Date()
    pickedDate.setFullYear(parseInt(parts[2], 10))
    pickedDate.setDate(parseInt(parts[0], 10))
    pickedDate.setMonth(parseInt(parts[1], 10)-1)
    pickedDate.setHours($("#editTimeDuty").val())

    todayDate = new Date();
    return pickedDate > todayDate
}

function dateFormat(){
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test($("#editDateDuty").val())){
        return false;
    }
    else{
        var parts = $("#editDateDuty").val().split("/");
        var day = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var year = parseInt(parts[2], 10);
        if(year < 1000 || year > 3000 || month == 0 || month > 12){
            return false;
        }
        else{
            var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
            if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
                monthLength[1] = 29;
            return day > 0 && day <= monthLength[month - 1];
        }
    }
}

function uploadDuty(duty){
    //date dd/mm/yyyy
    var splitDate = $("#editDateDuty").val().split('/')[2]+'-'+$("#editDateDuty").val().split('/')[1]+'-'+$("#editDateDuty").val().split('/')[0]
    var date = splitDate +" "+$("#editTimeDuty").val()+":00:00"
    var note = $("#editNoteDuty").val()
    var done = false
    var user = []

    $('input[type=checkbox]').each(function () {
        id = this.id.replace("_","@").replace("-",".")
        if (id != "editDoneDuty"){
            if ($(this).is(':checked'))
                user.push(id)
        }
    })
    if($("#editDoneDuty").is(':checked')){
         done = true
    }

    var dutyUpd = {
        date: date,
        note: note,
        done: done,
        homeTask: duty.homeTask.id,
        users: user,
    }

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/duty/"+duty.id+"/edit",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        data: JSON.stringify(dutyUpd),
        dataType: 'json',
        success: function (data) {
        }
    });


}