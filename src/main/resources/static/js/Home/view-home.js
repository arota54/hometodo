function deleteUser(email) {
    $.ajax({
        type: "DELETE",
        url: "/home/users/" + email + "/remove",
        success: function (data) {
            var obj = data;
            window.location = "/home";
            return false;
        },
        error: function (e) {
            window.location = "/home";
            return false;
        }
    });
}