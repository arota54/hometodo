$(document).ready(function() {
    $('#form').on("submit", function () {

        var house = {
            name : $("#houseName").val(),
            admin : $("#administratorId").val(),
            users: [$("#administratorId").val()],
        }

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/home/add",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                },
            data: JSON.stringify(house),
            dataType: 'json',
            success: function (data) {
                            var obj = data;
                            window.location = "/home";
                            return false;
                        },
            error: function (e) {
                    window.location = "/home";
                    return false;
                    }
        });
        return false;
    });
});
