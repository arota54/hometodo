/**
 *  Function used to disable or able the button to submit the email
 */
function stoppedTyping(){
    if(document.getElementById('userToAddEmail').value.length > 0) { //if it's not empty
        document.getElementById('add_user_submit').disabled = false;
    } else {
        document.getElementById('add_user_submit').disabled = true;
    }
}


/**
 * This function is needed to handle what happens when you click on a suggested email.
 * 1. Has to put in the input text
 * 2. Has to hide the suggestions
 * 3. Has to enable the button submit "Add user to home"
 *
 * @param email
 */
function onClickSuggestion(email) {
    // 1.
    $("#userToAddEmail").val(email);

    // 2.
    $("#result").hide();

    // 3.
    document.getElementById('add_user_submit').disabled = false;
}


/**
 * This function is used by showResults function.
 * @param input
 * @param search_terms
 * @returns {*[]|*}
 */
function autocompleteMatch(input, search_terms) {
    if (input == '') {
        return [];
    }
    var reg = new RegExp(input)
    return search_terms.filter(function(term) {
        if (term.match(reg)) {
            return term;
        }
    });
}


/**
 * This function is called whenever the administrator writes in the input box of the form.
 * @param val is the value writed inside the box and it is taken from the HTML.
 */
function showResults(val) {
    // Check if someone is writing something
    if(document.getElementById('userToAddEmail').value.length > 0) { //if it's not empty
        document.getElementById('add_user_submit').disabled = false;
    } else {
        document.getElementById('add_user_submit').disabled = true;
    }

    // If it has been hide before, just show the div
    $("#result").show();

    // This ajax code returns all the emails of the users with no home
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/add/user-in-a-home/get-all-users-with-no-home",
        contentType: 'application/json',
        success: function (data, status, xhr) {
            if (xhr.readyState === 4) {

                $(document).ajaxStop(function () {
                    emails = JSON.parse(xhr.responseText).emails.split(",");

                    res = document.getElementById("result");
                    res.innerHTML = '';
                    let list = '';
                    let terms = autocompleteMatch(val, emails);
                    for (i=0; i<terms.length; i++) {
                        list += '<li onclick="onClickSuggestion(this.innerHTML)">' + terms[i] + '</li>';
                    }
                    res.innerHTML = '<ul>' + list + '</ul>';

                    // Getting ride of the excideeing variable
                    //emails = undefined
                    delete window.emails
                })
            }else{
                "Error on something that is not the client.";
            }
        },
        dataType: "json"
    });
}


$(document).ready(function() {
    // Disabling the button to submit the new user
    document.getElementById('add_user_submit').disabled = true;
    document.getElementById('add_user_submit').addEventListener("keyup", stoppedTyping);

    // Hiding the suggestion div
    $("#result").hide();

    // This take care of adding a new user when the button submit it's clicked
    $('#form').on("submit", function () {

        var userToAdd = {emailUser: $("#userToAddEmail").val()};

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: "/add/user-to-home",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            data: JSON.stringify(userToAdd),
            success: function (data, status, xhr) {
                if (xhr.readyState === 4) {

                    $(document).ajaxStop(function () {
                        user_inserted = JSON.parse(xhr.responseText).user_inserted
                        if (user_inserted == 1) {
                            window.location = "/home";
                        } else if(user_inserted == -1) {
                            window.location = "/add/user-in-a-home";
                            alert("You inserted an email of a user that has already a home.");
                        } else if(user_inserted == -2) {
                            window.location = "/add/user-in-a-home";
                            alert("The email inserted doesn't belong to any existent user.");
                        } else if(user_inserted == 0) {
                            window.location = "/add/user-in-a-home";
                            alert("Error not predictable, contact the developers.");
                        }
                    })
                }else{
                    "Error on something that is not the client.";
                }

                return false;
            },
            error: function (request, status, errorThrown) {
                window.location = "/add/user-in-a-home";
                return false;
            }
        });
        return false;
    });
});