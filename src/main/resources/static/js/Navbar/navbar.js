/**
 * Show the right navbar: if a User is a User in a home they can see the entire menu, if they aren't, they can see
 * only the logout button in the left side menu.
 * @param user_cookie cookie in "email-hash" format
 */
function switch_navbar(user_cookie) {
    if(user_cookie != null) {

        first_score = user_cookie.indexOf("-");

        email = user_cookie.substring(0, first_score);
        hash = user_cookie.substring(first_score+1, user_cookie.length);

        $.ajax({
            type: "POST",
            url: "http://localhost:8080/navbar",
            contentType: 'application/json',
            data: '{"email": "' + email + '", "hash": "' + hash + '"}',
            success: function (data, status, xhr) {
                if (xhr.readyState === 4) {                    $(document).ajaxStop(function () {
                        if (JSON.parse(xhr.responseText).is_uh == 1) {
                            $(".navbar_user_UH").show();
                            $(".home_waiting_page").hide();
                        } else {
                            $(".navbar_user_UH").hide();
                            $(".home_waiting_page").show();
                        }
                    })
                }else{
                    "Error on something that is not the client.";
                }
            },
            dataType: "json"
        });
    }else{
        $(".navbar_user_UH").hide();
        $(".home_waiting_page").show();
    }
}


/**
 * When the HTML document is ready this function is called
 */
$( document ).ready(function() {
    switch_navbar(readCookie("user_cookie"));
});

