// id = YY-MM-DD
function goToDayView(id) {
    handleDayView(new Date(id.split('-')[0], id.split('-')[1] - 1, id.split('-')[2]));
}

// return the hour in this format "0h"
function padTo2Digits(num) {
    return String(num).padStart(2, '0');
}

// create and return an element with class = className
// element and className are strings
function createElementWithClass(element, className) {
    const div = document.createElement(element);
    div.className = className;
    return div;
}

function createElementWithClassType(element, className, typeName) {
    const div = document.createElement(element);
    div.className = className;
    div.type = typeName
    return div;
}

function isToday(month, day, year) {
    const today = new Date();
    return day === today.getDate() && month === today.getMonth() && year === today.getFullYear();
}

function showMonthView() {
    $("#weekView").hide();
    $("#headerWeekView").hide();
    $("#dayView").hide();
    $("#headerDayView").hide();

    $("#monthView").show();
    $("#headerMonthView").show();
}

function showWeekView() {
    $("#monthView").hide();
    $("#headerMonthView").hide();
    $("#dayView").hide();
    $("#headerDayView").hide();

    $("#weekView").show();
    $("#headerWeekView").show();
}

function showDayView() {
    $("#monthView").hide();
    $("#headerMonthView").hide();
    $("#weekView").hide();
    $("#headerWeekView").hide();

    $("#dayView").show();
    $("#headerDayView").show();
}

// set the calendarTitle as "MonthLong  Year"
function setCalendarTitle(title = new Date().toLocaleDateString('en-us', { month: 'long' }) + " " + new Date().getFullYear()) {
    $("#calendarTitle").html(title);
}

// compare two dates (ignoring the time)
// 0 = equal, -1 = date1 < date2, 1 = date1 > date2
function compareDate(date1, date2) {
    if(date1.getFullYear() < date2.getFullYear()) {
        return -1;
    }
    else if(date1.getFullYear() > date2.getFullYear()) {
        return 1;
    }
    else {
        if(date1.getMonth() < date2.getMonth()) {
            return -1;
        }
        else if(date1.getMonth() > date2.getMonth()) {
            return 1;
        }
        else {
            if(date1.getDate() < date2.getDate()) {
                return -1;
            }
            else if(date1.getDate() > date2.getDate()) {
                return 1;
            }
            else return 0;
        }
    }
}

/* particular case for:
    3/31 -> 4/31
    5/31 -> 6/31
    8/31 -> 9/31
    10/31 -> 11/31
 */
function changeMonth(nd, dateWeek) {
    var date;
    if (compareDate(nd, new Date(nd.getFullYear(), 2, 31)) === 0) {
        date = new Date(nd.getFullYear(), 3, 1);
    } else if (compareDate(nd, new Date(nd.getFullYear(), 4, 31)) === 0) {
        date = new Date(nd.getFullYear(), 5, 1);
    } else if (compareDate(nd, new Date(nd.getFullYear(), 7, 31)) === 0) {
        date = new Date(nd.getFullYear(), 8, 1);
    } else if (compareDate(nd, new Date(nd.getFullYear(), 9, 31)) === 0) {
        date = new Date(nd.getFullYear(), 10, 1);
    } else {
        date = new Date(nd.getFullYear(), dateWeek.getMonth(), nd.getDate())
    }
    return date;
}

function getPreviousSunday(date = new Date()) {
    const previousSunday = new Date(date);

    previousSunday.setDate(date.getDate() - date.getDay());

    return previousSunday;
}

function handleAddingNewDuty(id) {
    $("#addDutyModal").modal('show');
}

function checkCookie(user_cookie) {
    if(user_cookie != null) {
        let email_hash = user_cookie.split("-");

        console.log(email_hash[0]);
        console.log(email_hash[1]);
    }else{
        console.log("The user_cookie is not set");
    }
}
