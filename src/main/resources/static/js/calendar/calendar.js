let navMonth = 0;   // used for switching among the months
let navWeek = 0;    // used for switching among the weeks
let navDay = 0;     // used for switching among the days
const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
let view = "month";

// get the type of view and the nav value from href
if(window.location.href.split('?').length !== 1) {
    if(window.location.href.split('&').length !== 1) {
        view = window.location.href.split('&')[0].split('?')[1]

        switch (view) {
            case "month": {
                navMonth = parseInt(window.location.href.split('&')[1])
                break;
            }
            case "week": {
                navWeek = parseInt(window.location.href.split('&')[1])
                break;
            }
            case "day": {
                navDay = parseInt(window.location.href.split('&')[1])
                break;
            }
        }

    }
}

function handleMonthView() {
    // hide other views
    showMonthView();

    // set the other navs to 0 (so that the next time I'll switch to another view, I'll start from the current date
    navWeek = 0;
    navDay = 0;

    let dt = new Date();  // current date
    // set the month on the month I want to see (because dt return always the current date)
    if (navMonth !== 0) {
        // create a new Date as "(month+navMonth)-1-dt.getYear()
        dt = new Date(dt.getFullYear(), dt.getMonth() + navMonth, 1);
    }

    const month = dt.getMonth();    // 0-11
    const year = dt.getFullYear();

    const lastDayOfMonth = new Date(year, month + 1, 0); // 0 stands for the last day of the previous month
    const daysInMonth = lastDayOfMonth.getDate();
    const firstDayOfMonth = new Date(year, month, 1);

    const firstDayOfMonthDateString = firstDayOfMonth.toLocaleDateString('en-us', {
        weekday: 'long',
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
    }); // example: "Tuesday, 3/1/2022"

    const lastDayOfMonthDateString = lastDayOfMonth.toLocaleDateString('en-us', {
        weekday: 'long',
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
    }); // example: "Thursday, 3/31/2022"

    // number of days in the week before the first day of the month
    // example: if the 1th is tuesday, paddingDays is equal to 2 because before tuesday there are sunday and monday
    const paddingDaysBefore = weekdays.indexOf(firstDayOfMonthDateString.split(', ')[0]);

    // number of days in the week after the last day of the month
    // example: if the last is friday, paddingDays is equal to 1 because after there is only saturday
    const paddingDaysAfter = weekdays.length - weekdays.indexOf(lastDayOfMonthDateString.split(', ')[0]) - 1;

    setCalendarTitle(dt.toLocaleDateString('en-us', { month: 'long' }) + " " + dt.getFullYear());

    // clear the calendar div before adding new days
    $("#monthView").text("");
    // fill the calendar with days before the current month and with the days of the current month
    for(let i = 1; i <= paddingDaysBefore + daysInMonth; i++) {
        const day = createElementWithClass("div", "border day-monthview");

        // these are the days in the month
        if(i > paddingDaysBefore) {
            // TODO: see the day detail on onclick event
            const dayCurrentMonthNumberTitle = createElementWithClass("a", "a-current-month date-circle-month");

            if(i - paddingDaysBefore === 1) {
                dayCurrentMonthNumberTitle.innerText = (i - paddingDaysBefore + " " + (dt.toDateString().split(' ')[1]));
            }
            else {
                dayCurrentMonthNumberTitle.innerText = (i - paddingDaysBefore);
            }

            // id = "YY-MM-DD"
            var date = year + "-" + padTo2Digits(month + 1) + "-" + padTo2Digits(i - paddingDaysBefore);
            dayCurrentMonthNumberTitle.setAttribute("id", date);
            day.setAttribute("id", date);

            // let adding a new duty if the date is bigger than today
            if(compareDate(new Date(day.getAttribute("id")), new Date()) > 0) {
                day.addEventListener('click', function (e) {
                    if(e.target === this)
                        handleAddingNewDuty(day.getAttribute("id"), "", true)
                });
            }

            // if it is the current day then highlight the number
            if(isToday(month, (i - paddingDaysBefore), year)) {
                dayCurrentMonthNumberTitle.classList.add("date-circle-current-day");

                day.addEventListener('click', function (e) {
                    if(e.target === this)
                        handleAddingNewDuty(day.getAttribute("id"), "", true)
                });
            }

            dayCurrentMonthNumberTitle.addEventListener('click', () => goToDayView(dayCurrentMonthNumberTitle.getAttribute("id")));

            day.appendChild(dayCurrentMonthNumberTitle);
        }
        // these are the days of the previous month
        else {
            const dayOfMonthBefore = new Date(year, month, 0).getDate() - (paddingDaysBefore-i);
            const dayMonthBeforeNumberTitle = createElementWithClass("a", "a-not-current-month date-circle-month");
            dayMonthBeforeNumberTitle.innerText = dayOfMonthBefore;
            day.appendChild(dayMonthBeforeNumberTitle);

            // id = "YY-MM-DD"
            if(month === 0) {
                dayMonthBeforeNumberTitle.setAttribute("id", (year - 1) + "-" + 12 + "-" + padTo2Digits(dayOfMonthBefore));
                day.setAttribute("id", (year - 1) + "-" + 12 + "-" + padTo2Digits(dayOfMonthBefore));
            }
            else {
                dayMonthBeforeNumberTitle.setAttribute("id", year + "-" + padTo2Digits(month) + "-" + padTo2Digits(dayOfMonthBefore));
                day.setAttribute("id", year + "-" + padTo2Digits(month) + "-" + padTo2Digits(dayOfMonthBefore));
            }

            // let adding a new duty if the date is bigger than today
            if(compareDate(new Date(day.getAttribute("id")), new Date()) >= 0) {
                day.addEventListener('click', function (e) {
                    if(e.target === this)
                        handleAddingNewDuty(day.getAttribute("id"), "", true)
                });
            }

            dayMonthBeforeNumberTitle.addEventListener('click', () => goToDayView(dayMonthBeforeNumberTitle.getAttribute("id")));
        }

        $("#monthView").append(day);
    }

    // fill the calendar with days after the month
    for(let i = 0; i < paddingDaysAfter; i++) {
        const day = createElementWithClass("div", "border day-monthview");

        const dateMonthAfter = new Date(year, month+1, 1);
        const dayOfMonthAfter = dateMonthAfter.getDate() + i;

        const dayMonthAfterNumberTitle = createElementWithClass("a", "a-not-current-month date-circle-month");

        if(i === 0) {
            dayMonthAfterNumberTitle.innerText = (dayOfMonthAfter + " " + (dateMonthAfter.toDateString().split(' ')[1]));
        }
        else {
            dayMonthAfterNumberTitle.innerText = dayOfMonthAfter;
        }

        day.appendChild(dayMonthAfterNumberTitle);

        // id = "YY-MM-DD"
        if(month === 11) {
            dayMonthAfterNumberTitle.setAttribute("id", (year + 1) + "-" + 1 + "-" + padTo2Digits(dayOfMonthAfter));
            day.setAttribute("id", (year + 1) + "-" + 1 + "-" + padTo2Digits(dayOfMonthAfter));
        }
        else {
            dayMonthAfterNumberTitle.setAttribute("id", year + "-" + padTo2Digits((month + 1) % 12 + 1) + "-" + padTo2Digits(dayOfMonthAfter));
            day.setAttribute("id", year + "-" + padTo2Digits((month + 1) % 12 + 1) + "-" + padTo2Digits(dayOfMonthAfter));
        }

        // let adding a new duty if the date is bigger than today
        if(compareDate(new Date(day.getAttribute("id")), new Date()) >= 0) {
            day.addEventListener('click', function (e) {
                if(e.target === this)
                    handleAddingNewDuty(day.getAttribute("id"), "", true)
            });
        }

        dayMonthAfterNumberTitle.addEventListener('click', () => goToDayView(dayMonthAfterNumberTitle.getAttribute("id")));

        $("#monthView").append(day);
    }

    getAllDutiesMonthView(dt);

    return false;
}

function handleWeekView() {
    // hide other views
    showWeekView();

    navMonth = 0;
    navDay = 0;

    // used to switch the week
    // example: today is the 15th, dateWeek, the next week, will be 22th
    let dateWeek = new Date();

    // set the date on the week I want to see (because dateWeek return always the current date)
    if (navWeek !== 0) {
        dateWeek.setDate(new Date().getDate() + navWeek*7);
    }

    const month = dateWeek.getMonth();    // 0-11
    const year = dateWeek.getFullYear();


    const currentDayDateString = dateWeek.toLocaleDateString('en-us', {
        weekday: 'long',
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
    }); // example: "Tuesday, 3/29/2022"

    // example: "Tuesday"
    const dateWeekLongDay = currentDayDateString.split(', ')[0];

    let weekViewHeader = $("#headerWeekView > div");
    let nd = new Date(dateWeek);
    let firstDayOfTheWeek;
    let string;
    let hour;
    let id;
    let datesOfTheWeek = [] // contains the dates of the shown week

    // put in the first cell of the header the time zone
    weekViewHeader[0].innerText = nd.toString().split(' ')[5];

    setCalendarTitle(dateWeek.toLocaleDateString('en-us', { month: 'long' }) + " " + year);

    // clear the calendar div before adding new days
    $("#weekView").text("");



    // 24 rows: one hour each
    for(let i = 0; i <= 24; i++) {
        if(i > 0) {
            const dayHour = createElementWithClass("div", "hour-weekview align-self-center");

            hour = padTo2Digits(padTo2Digits(i-1) + ":" + padTo2Digits(0));
            dayHour.innerHTML = hour;
            dayHour.style.backgroundColor = "#f5f5f5";

            $("#weekView").append(dayHour);
        }

        // fill the row starting from the current date dateWeek
        for(let j = 1; j < weekViewHeader.length; j++) {
            // create the new date
            nd.setDate(dateWeek.getDate() - weekdays.indexOf(dateWeekLongDay) + j - 1);

            // fit the header with the days numbers and eventually change the calendar title
            if(i === 0) {
                datesOfTheWeek[j-1] = nd.getFullYear() + "-" + padTo2Digits(nd.getMonth() + 1) + "-" + padTo2Digits(nd.toString().split(' ')[2]);

                string = nd.toString().split(' ')[0] + "\n";
                weekViewHeader[j].innerText = string;

                const dayNumberTitle = createElementWithClass("a", "a-current-week date-circle-week");
                dayNumberTitle.innerText = nd.toString().split(' ')[2];
                dayNumberTitle.style.fontSize = "large";
                dayNumberTitle.setAttribute("id", datesOfTheWeek[j-1]);    // id = "mm/dd/yy"

                dayNumberTitle.addEventListener('click', () => goToDayView(dayNumberTitle.getAttribute("id")));

                // if it is the current day then highlight the number
                if(isToday(nd.getMonth(), parseInt(nd.toString().split(' ')[2]), nd.getFullYear())) {
                    dayNumberTitle.classList.add("date-circle-current-day");
                }

                weekViewHeader[j].appendChild(dayNumberTitle);

                // if nd < currentDay
                if(compareDate(nd, new Date()) === -1) {
                    dayNumberTitle.style.color = "grey";
                }

                // the month changes
                if (nd.getMonth() !== month) {
                    // clean the calendar title
                    setCalendarTitle("");

                    // the year changes
                    if (nd.getFullYear() !== year) {
                        // set the calendarTitle as "MonthShort Year - OtherMonthShort OtherYear"
                        if(nd.getFullYear() > year) {
                            setCalendarTitle(dateWeek.toString().split(' ')[1] + " " + year + " - " + nd.toString().split(' ')[1] + " " + nd.getFullYear());
                        }
                        else {
                            setCalendarTitle(nd.toString().split(' ')[1] + " " + nd.getFullYear() + " - " + dateWeek.toString().split(' ')[1] + " " + year);
                        }
                    } else {
                        // set the calendarTitle as "MonthShort - OtherMonthShort Year"
                        if(dateWeek.getMonth() > nd.getMonth())
                            setCalendarTitle(nd.toString().split(' ')[1] + " - " + dateWeek.toString().split(' ')[1] + " " + year);
                        else
                            setCalendarTitle(dateWeek.toString().split(' ')[1] + " - " + nd.toString().split(' ')[1] + " " + year);

                        // because the month increases itself if you give to setDate a day >= 1
                        nd = changeMonth(nd, dateWeek);
                    }
                }
            }
            else {
                // this div represents the hour of a specific day
                const dayHour = createElementWithClass("div", "border hour-weekview");

                // set the div id as: "wYY-MM-DD_hh-mm"
                id = "w" + datesOfTheWeek[j-1] + "_" + padTo2Digits(padTo2Digits(i-1) + "-" + padTo2Digits(0));
                dayHour.setAttribute("id", id);

                // let adding a new duty if the date is bigger than today
                if(compareDate(new Date(datesOfTheWeek[j-1]), new Date()) > 0) {
                    dayHour.addEventListener('click', function (e) {
                        if (e.target === this)
                            handleAddingNewDuty(datesOfTheWeek[j-1], (i-1), false)
                    });
                }
                else if(compareDate(new Date(datesOfTheWeek[j-1]), new Date()) === 0) {
                    if((i-1) > new Date().getHours()) {
                        dayHour.addEventListener('click', function (e) {
                            if (e.target === this)
                                handleAddingNewDuty(datesOfTheWeek[j-1], (i-1), false)
                        });
                    }
                }

                $("#weekView").append(dayHour);
            }
        }
    }

    getAllDutiesWeekView(getPreviousSunday(dateWeek));

    return false;
}

// if it was called from clicking the button: shows the current date
// if it was called from clicking a specific day in either month or week view: show that day
function handleDayView(date = new Date()) {
    // hide other views
    showDayView();

    navMonth = 0;
    navWeek = 0;

    if(navDay !== 0) {
        date.setDate(date.getDate() + navDay);
    }

    const dayStringForTitle = date.toLocaleDateString('en-us', {
        weekday: 'long',
        year: 'numeric',
        month: 'numeric',
        day: 'numeric',
    }); // example: "Tuesday, 3/29/2022"

    // set the calendarTitle as "Weekday, mm/dd/yy"
    setCalendarTitle(dayStringForTitle);

    // clear the calendar div before adding new days
    $("#dayView").text("");

    let dayViewHeader = $("#headerDayView > div");
    let string;
    let id;
    let hourString;
    let dateString = date.getFullYear() + "-" + padTo2Digits(date.getMonth() + 1) + "-" + padTo2Digits(date.toString().split(' ')[2]);

    // put in the first cell of the header the time zone
    //dayViewHeader[0].innerText = date.toString().split(' ')[5];

    string = date.toString().split(' ')[0] + "\n";
    dayViewHeader[0].innerText = string;

    const dayNumberTitle = createElementWithClass("a", "a-current-week date-circle-week");
    dayNumberTitle.innerText = date.toString().split(' ')[2]
    dayNumberTitle.style.fontSize = "large";
    dayNumberTitle.setAttribute("id", dateString);    // id = "YY-MM-DD"

    // if it is the current day then highlight the number
    if(isToday(date.getMonth(), parseInt(date.toString().split(' ')[2]), date.getFullYear())) {
        dayNumberTitle.classList.add("date-circle-current-day");
    }

    // if nd < currentDay
    if(compareDate(date, new Date()) === -1) {
        dayNumberTitle.style.color = "grey";
    }

    dayViewHeader[0].appendChild(dayNumberTitle);

    let div;

    // 24 rows: one hour each
    for(let i = 1; i <= 24; i++) {
        div = createElementWithClass("div", "row hour-dayview")
        // create the div for the hours
        const hour = createElementWithClass("div", "col col-lg-3 align-self-center");
        hourString = padTo2Digits(padTo2Digits(i-1) + ":" + padTo2Digits(0));
        hour.innerHTML = hourString;

        div.append(hour);

        // create the div for putting the elements in
        //const day = createElementWithClass("div", "col border d-flex justify-content-center");
        const day = createElementWithClass("div", "col border");
        //day.style.height = "45px"
        // set the div id as: "dYY-MM-DD_hh:mm"
        id = "d" + dateString + "_" + padTo2Digits(i-1) + "-" + padTo2Digits(0);
        day.style.backgroundColor = "white";
        day.setAttribute("id", id);


        // let adding a new duty if the date is bigger than today
        if(compareDate(new Date(dateString), new Date()) > 0) {
            day.addEventListener('click', function (e) {
                if (e.target === this)
                    handleAddingNewDuty(dateString, (i-1), false)
            });
        }
        else if(isToday(date.getMonth(), parseInt(date.toString().split(' ')[2]), date.getFullYear())) {
            if((i-1) > new Date().getHours()) {
                day.addEventListener('click', function (e) {
                    if (e.target === this)
                        handleAddingNewDuty(dateString, (i-1), false)
                });
            }
        }

        div.append(day)
        $("#dayView").append(div);

    }

    getAllDutiesDayView(date);

    return false;
}

function handleSwitchEvents() {
    $("#todayButton").click(function () {
        // go to the current month
        if(!$("#monthView").is(":hidden")) {
            navMonth = 0;
            handleMonthView();
        }

        // go to the current week
        if(!$("#weekView").is(":hidden")) {
            navWeek = 0;
            handleWeekView();
        }

        // go to the current day
        if(!$("#dayView").is(":hidden")) {
            navDay = 0;
            handleDayView();
        }
    });

    $("#nextButton").click(function () {
        // go to the next month
        if(!$("#monthView").is(":hidden")) {
            navMonth++;
            handleMonthView();
        }

        // go to the next week
        if(!$("#weekView").is(":hidden")) {
            navWeek++;
            handleWeekView();
        }

        // go to the next day
        if(!$("#dayView").is(":hidden")) {
            navDay++;
            handleDayView();
        }
    });

    $("#backButton").click(function () {
        // go to the previous month
        if(!$("#monthView").is(":hidden")) {
            navMonth--;
            handleMonthView();
        }

        // go to the previous week
        if(!$("#weekView").is(":hidden")) {
            navWeek--;
            handleWeekView();
        }

        // go to the previous day
        if(!$("#dayView").is(":hidden")) {
            navDay--;
            handleDayView();
        }
    });

    $("#monthButton").click(function () {
        // show month view
        if($("#monthView").is(":hidden")) {
            //showMonthView();
            view = "month";
            handleMonthView();
        }
    });

    $("#weekButton").click(function () {
        // show week view
        if($("#weekView").is(":hidden")) {
            //showWeekView();
            view = "week"
            handleWeekView();
        }
    });

    $("#dayButton").click(function () {
        // show week view
        if($("#dayView").is(":hidden")) {
            //showDayView();
            view = "day"
            handleDayView();
        }
    });

    return false;
}

function handleView() {
    if(view === "month") {
        showMonthView();
        handleMonthView();
    }
    else if(view === "week") {
        showWeekView();
        handleWeekView();
    }
    else {
        showDayView();
        handleDayView();
    }
}

$(document).ready(function() {
    handleView();

    handleSwitchEvents();

    // redirect when addDutyModal is hidden considering the previous visualization
    $('#addDutyModal').on('hidden.bs.modal', function () {
        var string = window.location.href.split('?')[0] + "?" + view;

        switch (view) {
            case "month": {
                string = string + "&" + navMonth;
                break;
            }
            case "week": {
                string = string +  "&" + navWeek;
                break;
            }
            case "day": {
                string = string +  "&" + navDay;
                break;
            }
        }


        window.location.href = string;

        handleView();
    });

     $('#dutyDetailModal').on('hidden.bs.modal', function () {
            var string = window.location.href.split('?')[0] + "?" + view;

            switch (view) {
                case "month": {
                    string = string + "&" + navMonth;
                    break;
                }
                case "week": {
                    string = string +  "&" + navWeek;
                    break;
                }
                case "day": {
                    string = string +  "&" + navDay;
                    break;
                }
            }


            window.location.href = string;

            handleView();
        });

    return false;
});