// print all the duties in the month view
function getAllDutiesMonthView(d) {
    let months = d.getMonth() + 1
    let urlM = d.getFullYear() + '-' + months + '-' + d.getDate();
    let divDay

    $.ajax({
        url: "/schedule/month/"+urlM,
        success: function(result){
            for(const duty of result){
                divDay='#'+duty.date.split(' ')[0]

                const divDuty = createElementWithClass("div", "event");
                divDuty.innerText = duty.homeTask.name
                divDuty.setAttribute("id",duty.id)
                divDuty.addEventListener('click', () => getDutyDetail(divDuty.getAttribute("id")));

                $(divDay).append(divDuty);
            }
        }
    });
}

// print all the duties in the day view
function getAllDutiesDayView(d) {
    let months = d.getMonth() + 1
    let urlD= d.getFullYear() + '-' + months + '-' + d.getDate();
    let divDayHour

    $.ajax({
        url: "/schedule/day/"+urlD,
        success: function(result) {
            for(const duty of result) {
                divDayHour='#d'+duty.date.split('.')[0]
                divDayHour=divDayHour.replace(" ","_")
                divDayHour=divDayHour.replace(":","-")
                divDayHour=divDayHour.substring(0, divDayHour.length - 3);

                const divDuty = createElementWithClass("div", "event");
                divDuty.innerText = duty.id + ": " + duty.homeTask.name
                divDuty.setAttribute("id",duty.id)
                divDuty.addEventListener('click', () => getDutyDetail(divDuty.getAttribute("id")));
                divDuty.style.width = "100%";

                $(divDayHour).append(divDuty);
            }
        }
    });
}

// print all the duties in the week view
function getAllDutiesWeekView(d) {
    let months = d.getMonth() + 1
    let urlD= d.getFullYear() + '-' + months + '-' + d.getDate();
    let divDayHour
    $.ajax({
        url: "/schedule/week/"+urlD,
        success: function(result){
            for(const duty of result){
                divDayHour='#w'+duty.date.split('.')[0]

                divDayHour=divDayHour.replace(" ","_")
                divDayHour=divDayHour.replace(":","-")
                divDayHour=divDayHour.substring(0, divDayHour.length - 3);

                const divDuty = createElementWithClass("div", "event");
                divDuty.innerText = duty.id + ": " + duty.homeTask.name
                divDuty.setAttribute("id",duty.id)
                divDuty.addEventListener('click', () => getDutyDetail(divDuty.getAttribute("id")));

                $(divDayHour).append(divDuty);
            }
        }
    });
}
// print all the duty detail in the modal
function getDutyDetail(id) {
    $.ajax({
        url: "/schedule/"+id,
        success: function(duty) {
            printInfoDutyInModal(duty);
            return duty;
        }
    });
    return null;
}